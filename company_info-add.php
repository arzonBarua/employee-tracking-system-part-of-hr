<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";


$id = isset($_GET['id']) ? $_GET['id'] : "";

if(!empty($id)){
	$result = mysqli_query($con, "SELECT * FROM company_info WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result);
}


$queryGetComDivision = mysqli_query($con,"SELECT * FROM company_division_info ORDER BY id");
$comdivisionList     = array();
while($rowList = mysqli_fetch_assoc($queryGetComDivision)){
	$comdivisionList[$rowList['id']] = $rowList['division_name'];
}


?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Company Information:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Add Company Information</h4>
				</div>
				<div class="panel-body panel-form">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>

					<form action="actions/beximco_report/view.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Company Division</label>
                            <div class="col-md-9">

                                <select name="division_id" class="form-control" required>
                                    <option value="">Select Company Division</option>
									<?php foreach($comdivisionList as $key => $val): ?>
                                        <option value="<?php echo $key ?>" <?php if(isset($row['division_id'])) { echo ($row['division_id']==$key) ? 'selected' : ''; } ?>><?php echo $val ?></option>
									<?php endforeach; ?>
                                </select>
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-3 control-label">Company Name</label>
							<div class="col-md-9">
                                <input type="hidden" name="id" value="<?php echo $id ?>">
								<input type="text" name="company_name" value="<?php echo isset($row['company_name']) ? $row['company_name'] : @$_POST['company_name'] ?>" class="form-control" placeholder="Company Name" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Company Short Name</label>
							<div class="col-md-9">
								<input type="text" name="company_short_name" value="<?php echo isset($row['company_short_name']) ? $row['company_short_name'] : @$_POST['company_short_name'] ?>" class="form-control" placeholder="Company Short Name" required/>
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Registered Address</label>
                            <div class="col-md-9">
                                <textarea name="registered_addr" class="form-control" placeholder="Registered Address" required><?php echo isset($row['registered_addr']) ? $row['registered_addr'] : @$_POST['registered_addr']?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Operational Address</label>
                            <div class="col-md-9">
                                <textarea name="operational_addr" class="form-control" placeholder="Operational Address" required><?php echo isset($row['operational_addr']) ? $row['operational_addr'] : @$_POST['operational_addr']?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Factory Address</label>
                            <div class="col-md-9">
                                <textarea name="factory_addr" class="form-control" placeholder="Factory Address" required><?php echo isset($row['factory_addr']) ? $row['factory_addr'] : @$_POST['factory_addr']?></textarea>
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-3 control-label">Status</label>
							<div class="col-md-9">
								<label class="radio-inline">
									<input type="radio" name="status" value="1" <?php if(isset($row['status'])){ echo ($row['status'] == '1') ? "checked" : ""; } ?> >
									Yes
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" value="0" <?php if(isset($row['status'])){ echo ($row['status'] == '0') ? "checked" : ""; } ?>>
									No
								</label>
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Status Change Date</label>
                            <div class="col-md-9">
                                <input type="text" name="status_change_date" value="<?php echo isset($row['status_change_date']) ? date("m/d/Y",strtotime($row['status_change_date'])) : ''?>" class="form-control datepicker" placeholder="Status Change Date" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Company Logo</label>
                            <div class="col-md-9">
                                <input type="file" name="company_logo" class="form-control" /><span style="color:red">*File Type: jpg, png, jepg</span>
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-9">
								<button type="submit" name="submit" class="btn btn-sm btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->
</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script>
    $('.datepicker').datepicker({
        format: "mm/dd/yyyy"
    }).on('change',function(ev){
        $(this).datepicker('hide');
    });
</script>
<?php include_once "inc/footer.php" ?>
