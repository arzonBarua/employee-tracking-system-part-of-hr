<?php
$del_sql = "delete from ttg";
$del_result = mysqli_query($con, $del_sql);

$sql = "select *from ticker where board='PUBLIC' and category!='Z' and deviation>0 order by  deviation DESC,inst ASC LIMIT 0,10";
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_assoc($result)) {

	$inst       = $row['inst'];
	$openp      = $row['openp'];
	$high       = $row['high'];
	$low        = $row['low'];
	$ltp        = $row['ltp'];
	$dvtn_nr    = $row['deviation'];
	$dvtn       = round($dvtn_nr, 4);

	$insert_sql     = "INSERT INTO ttg (inst, openp, high, low, ltp, dvtn) VALUES ('$inst', '$openp', '$high', '$low', '$ltp', '$dvtn')";
	$insert_result  = mysqli_query($con,$insert_sql);

}

/////Top ten gainer by percentage change
$del_sql    = "delete from ttg_perChg";
$del_result = mysqli_query($con,$del_sql);

$sql_per    = "select *from ticker where board='PUBLIC' and category!='Z' and percent_deviation>0 order by   percent_deviation DESC,inst ASC LIMIT 0,10";

$result_per = mysqli_query($con,$sql_per);
while ($row_per = mysqli_fetch_assoc($result_per)) {

	$inst = $row_per['inst'];
	$closep = $row_per['closep'];
	$high = $row_per['high'];
	$low = $row_per['low'];
	$ycp = $row_per['ycp'];
	$dvtn_nr = $row_per['percent_deviation'];
	$dvtn = round($dvtn_nr, 4);

	if ($dvtn > 0) {
		$insert_sql     = "INSERT INTO ttg_perChg (inst, closep, high, low, ycp, per_change) VALUES ('$inst', '$closep', '$high', '$low', '$ycp', '$dvtn')";
		$insert_result  = mysqli_query($con,$insert_sql);
	}

}


$del_sql    = "delete from ttl";
$del_result = mysqli_query($con,$del_sql);    // delete from ttl table

$sql        = "select *from ticker where board='PUBLIC' and deviation!= -100.0 order by  deviation ASC,inst ASC LIMIT 0,10";
$result     = mysqli_query($con,$sql);

while ($row = mysqli_fetch_assoc($result)) {

	$inst       = $row['inst'];
	$openp      = $row['openp'];
	$high       = $row['high'];
	$low        = $row['low'];
	$ltp        = $row['ltp'];
	$dvtn_nr    = $row['deviation'];
	$dvtn       = round($dvtn_nr, 4);


	$insert_sql = "INSERT INTO ttl (inst, openp, high, low, ltp, dvtn) VALUES ('$inst', '$openp', '$high', '$low', '$ltp', '$dvtn')";
	$insert_result = mysqli_query($con,$insert_sql);          // insert data into top ten looser(ttl) table


}

/////////top ten loser by percentage of deviation
$del_sql    = "delete from ttl_perChg";
$del_result = mysqli_query($con,$del_sql);    // delete from ttl table

$sql_ttl    = "select *from ticker where board='PUBLIC' and percent_deviation!= -100.0 order by  percent_deviation ASC,inst ASC LIMIT 0,10";
$result_ttl = mysqli_query($con,$sql_ttl);

while ($row_ttl = mysqli_fetch_assoc($result_ttl)) {

	$inst = $row_ttl['inst'];
	$closep = $row_ttl['closep'];
	$high = $row_ttl['high'];
	$low = $row_ttl['low'];
	$ycp = $row_ttl['ycp'];
	$dvtn_nr = $row_ttl['percent_deviation'];
	$dvtn = round($dvtn_nr, 4);


	$insert_sql = "INSERT INTO ttl_perChg (inst, closep, high, low, ycp, per_change) VALUES ('$inst', '$closep', '$high', '$low', '$ycp', '$dvtn')";
	$insert_result = mysqli_query($con,$insert_sql);          // insert data into top ten looser(ttl) table


}
?>
