<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";
?>
<style type="text/css">
.irs {
    height: 35px !important;
}
</style>
<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Beximco Employee Information:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-12">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" _data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<!-- <div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div> -->
					<h4 class="panel-title">Search Beximco Employee Information</h4>
				</div>
				<div class="panel-body">
					<form action="" method="POST">
						<div class="col-md-12">
							<div class="col-md-3">
								<label>Company</label>
								<ul id="company" class="inverse company_id">
									<?php
									$companyList = "";
	                                $sql = "SELECT * FROM `company_info`";
	                                $qry = mysqli_query($con, $sql);
	                                while($data = mysqli_fetch_object($qry)){
	                                	$companyList[] = '"'.$data->company_name.'"';
	                                }
	                                $companyList = implode(", ", $companyList);
	                                
	                                if(isset($_POST['company'])) {
										foreach ($_POST['company'] as $key => $value) {?>
											<li><?php echo $value;?></li>
										<?php 
										}
									}?>
	                            </ul>
	                            <hr/>

	                            <label>Department</label>
								<ul id="department" class="inverse dept_id">
									<?php
									$departmentList = "";
	                                $sql = "SELECT * FROM `department_info`";
	                                $qry = mysqli_query($con, $sql);
	                                while($data = mysqli_fetch_object($qry)){
	                                	$departmentList[] = '"'.$data->department_name.'"';
	                                }
	                                $departmentList = implode(", ", $departmentList);
	                                
	                                if(isset($_POST['department'])) {
										foreach ($_POST['department'] as $key => $value) {?>
											<li><?php echo $value;?></li>
										<?php 
										}
									}?>
	                            </ul>
								<!-- <select name="dept_id" class="form-control" required>
	                                <option value="">-- Select Department --</option>
	                                <?php
	                                $sql = "SELECT * FROM `department_info`";
	                                $qry = mysqli_query($con, $sql);
	                                while($data = mysqli_fetch_object($qry)){?>
	                                <option value="<?php echo $data->id?>"><?php echo $data->department_name;?></option>
	                                <?php }?>
	                            </select> -->
	                            <hr/>

	                            <label>Designation</label>
	                            <ul id="designation" class="inverse designation_id">
									<?php
									$designationList = "";
	                                $sql = "SELECT * FROM `designation_info`";
	                                $qry = mysqli_query($con, $sql);
	                                while($data = mysqli_fetch_object($qry)){
	                                	$designationList[] = '"'.$data->designation_name.'"';
	                                }
	                                $designationList = implode(", ", $designationList);
	                                if(isset($_POST['designation'])) {
										foreach ($_POST['designation'] as $key => $value) {?>
											<li><?php echo $value;?></li>
										<?php 
										}
									}
	                                ?>
	                            </ul>
	                            <!-- <select name="designation_id" class="form-control" required>
	                                <option value="">-- Select Designation --</option>
	                                <?php
	                                $sql = "SELECT * FROM `designation_info`";
	                                $qry = mysqli_query($con, $sql);
	                                while($data = mysqli_fetch_object($qry)){?>
	                                <option value="<?php echo $data->id?>"><?php echo $data->designation_name;?></option>
	                                <?php }?>
	                            </select> -->
							</div>
							<div class="col-md-3">
								<label>Age</label>
								<input type="text" id="_rangeSlider" name="_rangeSlider" value="<?php echo isset($_POST['_rangeSlider']) ? $_POST['_rangeSlider'] : '25;50';?>" />
								<hr/>

								<label>Service Year</label>
								<input type="text" id="_serviceYear" name="_serviceYear" value="<?php echo isset($_POST['_serviceYear']) ? $_POST['_serviceYear'] : '5;30';?>" />
								<hr/>

								<label>Employee Name</label>
								<input type="text" name="employee_name" class="form-control" value="<?php echo (isset($_POST['employee_name']) ? $_POST['employee_name'] : '');?>" id="">
							</div>
							<div class="col-md-3">
								<label>Blood Group</label>
								<ul id="bloodGroup" class="inverse">
								<?php 
								if(isset($_POST['bloodGroup'])) {
									foreach ($_POST['bloodGroup'] as $key => $value) {?>
										<li><?php echo $value;?></li>
									<?php 
									}
								}?>
	                            </ul>
								<!-- <select class="form-control input-sm" name="blood_group" required="">
	                                <option value="">-- Select Blood Group --</option>
	                                <option value="ab+">AB+</option>
	                                <option value="ab-">AB-</option>
	                                <option value="a+">A+</option>
	                                <option value="a-">A-</option>
	                                <option value="b+">B+</option>
	                                <option value="b-">B-</option>
	                                <option value="o+">O+</option>
	                                <option value="o-">O-</option>
	                            </select> -->
	                            <hr/>

	                            <label>Gender</label>
	                            <ul id="gender" class="inverse">
                            	<?php 
								if(isset($_POST['gender'])) {
									foreach ($_POST['gender'] as $key => $value) {?>
										<li><?php echo $value;?></li>
									<?php 
									}
								}?>
	                            </ul>
	                            <hr/>
	                            <!-- <select class="form-control input-sm" name="gender" required="">
		                            <option value="">-- Select Gender --</option>
		                            <option value="male">Male</option>
		                            <option value="female">Female</option>
		                        </select> -->

		                        <label>&nbsp;</label><br/>
								<input type="submit" value="Search" class="btn btn-primary" name="search" />
							</div>
							
							<div class="col-md-3">
								<label>Marital Status</label>
								<ul id="marital_status" class="inverse">
								<?php 
								if(isset($_POST['marital_status'])) {
									foreach ($_POST['marital_status'] as $key => $value) {?>
										<li><?php echo $value;?></li>
									<?php 
									}
								}?>
	                            </ul>
								<!-- <select class="form-control marital_status input-sm" name="marital_status" required="">
	                                <option value="">-- Select Marital Status --</option>
	                                <option value="Single">Single</option>
	                                <option value="Married">Married</option>
	                                <option value="Divorced">Divorced</option>
	                                <option value="Separated">Separated</option>
	                                <option value="Widowed">Widowed</option>
	                            </select> -->
	                            <hr/>

	                            <label>Religion</label>
	                            <ul id="religion" class="inverse">
	                            <?php 
								if(isset($_POST['religion'])) {
									foreach ($_POST['religion'] as $key => $value) {?>
										<li><?php echo $value;?></li>
									<?php 
									}
								}?>
	                            </ul>
	                            <!-- <select class="form-control input-sm" name="religion" required="">
	                                <option value="">-- Select Religion --</option>
	                                <option value="Islam">Islam</option>
	                                <option value="Hindu">Hindu</option>
	                                <option value="Buddhism">Buddhism</option>
	                                <option value="Christian">Christian
	                                </option>
	                            </select> -->
							</div>
						</div>
					</form>

					<?php
					function findName($id, $tbl, $con){
						$fieldName = $tbl."_name";
						$tbl = $tbl."_info";

						$data = mysqli_fetch_object(mysqli_query($con, "SELECT $fieldName as fieldName FROM $tbl WHERE id = '".$id."'"));
						return $data->fieldName;
					}

					function findID($name, $tbl, $con){
						$fieldName = $tbl."_name";
						$tbl = $tbl."_info";
						
						$name = "'".implode("','", $name)."'";
						$sql = "SELECT group_concat(id) increID FROM $tbl WHERE $fieldName IN (".$name.")";

						$data = mysqli_fetch_object(mysqli_query($con, $sql));
						return $data->increID;
					}
					if(isset($_POST['search'])){?>
					<div class="col-md-12" style="margin-top: 50px;">
						<button class="btn btn-primary pull-right" id="PrintDiv" onclick="printDiv('searchResultPage')" >Print</button>
						<div class="searchResultPage" id="searchResultPage">
							<h4>Beximco Employee Information</h4>
							<h5>Employee Search</h5>							
							<?php 
							$resultArr = array();
							$whereClause = "";
							if(!empty($_POST['company'])){
								$resultArr[] = "<strong>Company</strong>: ".implode(" , ", $_POST['company']);

								$whereClause[] = "company_id IN (".findID($_POST['company'], 'company', $con).")";
							}
							
							if(!empty($_POST['department'])){
								$resultArr[] = "<strong>Department</strong>: ".implode(" , ", $_POST['department']);

								$whereClause[] = "dept_id IN (".findID($_POST['department'], 'department', $con).")";
							}
							
							if(!empty($_POST['designation'])){
								$resultArr[] = "<strong>Designation</strong>: ".implode(" , ", $_POST['designation']);

								$whereClause[] = "designation_id IN (".findID($_POST['designation'], 'designation', $con).")";
							}

							if(!empty($_POST['_rangeSlider'])){
								$_rangeSlider = explode(";", $_POST['_rangeSlider']);
								$resultArr[] = "<strong>Age</strong>: ".implode(" - ", $_rangeSlider);

								$_rangeSliderStart = date("Y-m-d", strtotime("- ".$_rangeSlider[1]." years"));
								$_rangeSliderEnd = date("Y-m-d", strtotime("- ".$_rangeSlider[0]." years"));

								$whereClause[] = "dob BETWEEN '".$_rangeSliderStart."' AND '".$_rangeSliderEnd."'";
							}

							if(!empty($_POST['_serviceYear'])){
								$_serviceYear = explode(";", $_POST['_serviceYear']);
								$resultArr[] = "<strong>Service Year</strong>: ".implode(" - ", $_serviceYear);

								$_serviceYearStart = date("Y-m-d", strtotime("- ".$_serviceYear[1]." years"));
								$_serviceYearEnd = date("Y-m-d", strtotime("- ".$_serviceYear[0]." years"));

								$whereClause[] = "join_in_date BETWEEN '".$_serviceYearStart."' AND '".$_serviceYearEnd."'";	
							}

							if(!empty($_POST['bloodGroup'])){
								$resultArr[] = "<strong>Blood Group</strong>: ".implode(" , ", $_POST['bloodGroup']);
								$_bloodGroup = '"'.implode('","', array_map('strtolower', $_POST['bloodGroup'])).'"';
								$whereClause[] = "lower(blood_group) IN (".$_bloodGroup.")";
							}
							
							if(!empty($_POST['gender'])){
								$resultArr[] = "<strong>Gender</strong>: ".implode(" , ", $_POST['gender']);
								$_gender = '"'.implode('","', array_map('strtolower', $_POST['gender'])).'"';
								$whereClause[] = "lower(gender) IN (".$_gender.")";
							}
							
							if(!empty($_POST['marital_status'])){
								$resultArr[] = "<strong>Marital Status</strong>: ".implode(" , ", $_POST['marital_status']);
								$_marital_status = '"'.implode('","', array_map('strtolower', $_POST['marital_status'])).'"';
								$whereClause[] = "lower(marital_status) IN (".$_marital_status.")";
							}
							
							if(!empty($_POST['religion'])){
								$resultArr[] = "<strong>Religion</strong>: ".implode(" , ", $_POST['religion']);
								$_religion = '"'.implode('","', array_map('strtolower', $_POST['religion'])).'"';
								$whereClause[] = "lower(religion) IN (".$_religion.")";
							}
							
							if(!empty($_POST['employee_name'])){
								$resultArr[] = "<strong>Employee Name</strong>: ".$_POST['employee_name'];

								$whereClause[] = "(first_name LIKE '%".$_POST['employee_name']."%' OR middle_name LIKE '%".$_POST['employee_name']."%' OR last_name LIKE '%".$_POST['employee_name']."%')";
							}
							echo implode("<br/>", $resultArr);
							if(!empty($whereClause)){
								$whereClause = "WHERE ".implode(" AND ", $whereClause);
							}
							?>
							
							<style type="text/css">
								@media print {
									/*@page { size: landscape; }

								  	body * {
								    	visibility: hidden;								    	
								    	margin: 0;
								    	padding: 0;
								  	}
								  	#searchResultPage, #searchResultPage * {
								    	visibility: visible;
								  	}
								  	#searchResultPage {
									    position: absolute !important;
									    left: 0;
									    top: -600;
								  	}*/
								  	
								  	#PrintDiv {
								      	display: none;
								   	}

								}

							   	.employeeSearchResulttable {
									border-collapse: collapse;
									border: 1px solid #000;
									width: 1000px;
									margin-top: 20px;
								}
								.employeeSearchResulttable td, .employeeSearchResulttable th{
									padding: 5px;
									font-size: 11px;
								}
							</style>
							<table class="employeeSearchResulttable" border="1" cellpadding="5" width="1000">
								<tr>
									<th width="70">Employee ID</th>
									<th width="70">Existing ID</th>
									<th width="80">Employee Name</th>
									<th width="100">Company Name</th>
									<th width="70">Department</th>
									<th width="70">Designation</th>
									<th width="60">Joining Date</th>
									<th width="50">Gender</th>
									<th width="50">Marital Status</th>
									<th width="50">Religion</th>
									<th width="50">DOB</th>
									<th width="50">Blood Group</th>
									<th width="50">Reference Emp ID</th>
									<th width="60">Reference Name</th>
									<th width="50">Status</th>
								</tr>
								<?php 
								$statusArr = array("Inactive", "Active");
								$sql = "SELECT * FROM employee_info ".$whereClause;
								//echo "<br/><br/>".$sql."<br/>";
								$qry = mysqli_query($con, $sql);
								$numRow = mysqli_num_rows($qry);
								if($numRow > 0){
									while ($data = mysqli_fetch_object($qry)) {?>
									<tr>
										<td class="text-center-align"><?php echo $data->emp_id;?></td>
										<td class="text-center-align"><?php echo $data->existing_id;?></td>
										<td><?php echo $data->first_name." ".$data->middle_name." ".$data->last_name;?></td>
										<td><?php echo findName($data->company_id, 'company', $con);?></td>
										<td><?php echo findName($data->dept_id, 'department', $con);?></td>
										<td><?php echo findName($data->designation_id, 'designation', $con);?></td>
										<td><?php echo date("d-M-y", strtotime($data->join_in_date));?></td>
										<td><?php echo ucfirst($data->gender);?></td>
										<td><?php echo ucfirst($data->marital_status);?></td>
										<td><?php echo ucfirst($data->religion);?></td>
										<td><?php echo date("d-M-y", strtotime($data->dob));?></td>
										<td><?php echo strtoupper($data->blood_group);?></td>
										<td><?php echo $data->reference_emp_id;?></td>
										<td><?php echo $data->reference_name;?></td>
										<td class="text-center-align"><?php echo $statusArr[$data->status];?></td>
									</tr>
									<?php
									}
								} else {?>
								<tr>
									<th colspan="15" style="text-align: center;">No Data</th>
								</tr>
								<?php }?>
							</table>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->


<?php include_once "inc/footer-link.php" ?>
<script type="text/javascript" scr="actions/common-operation.js"></script>

<script>
    $(function(){
    	var __this = null;
    	$("#_rangeSlider").ionRangeSlider({min:20,max:80,type:"double",prefix:"",maxPostfix:"",prettify:!1,hasGrid:!1})
    	
    	$("#_serviceYear").ionRangeSlider({min:1,max:50,type:"double",prefix:"",maxPostfix:"",prettify:!1,hasGrid:!1})

    	$("#bloodGroup").tagit({
            availableTags: [ "AB+", "AB-", "A+", "A-", "B+", "B-", "O+", "O-"],
            fieldName: 'bloodGroup[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Blood Group"
        })

        $("#gender").tagit({
            availableTags: [ "Male", "Female"],
            fieldName: 'gender[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Gender"
        })

        $("#company").tagit({
            availableTags: [ <?php echo $companyList;?> ],
            fieldName: 'company[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Company"
        })

        $("#department").tagit({
            availableTags: [ <?php echo $departmentList;?> ],
            fieldName: 'department[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Department"
        })


        $("#designation").tagit({
            availableTags: [ <?php echo $designationList;?> ],
            fieldName: 'designation[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Designation"
        })


        $("#marital_status").tagit({
            availableTags: [ "Single", "Married", "Divorced", "Separated", "Widowed" ],
            fieldName: 'marital_status[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Maritual Status"
        })


        $("#religion").tagit({
            availableTags: [ "Islam", "Hindu", "Buddhism", "Christian" ],
            fieldName: 'religion[]',
            showAutocompleteOnFocus: !0,
            singleField: !1,
            placeholderText: "Select Religion"
        })
    })

	function printDiv(divId) {
		document.getElementById("PrintDiv").style.visibility = "hidden";
	    var content = document.getElementById(divId).innerHTML;
	    document.getElementById("PrintDiv").style.visibility = "visible";
	    var mywindow = window.open('', 'Print', 'height=900,width=1200');

	    mywindow.document.write('<html><head><title>Print</title><style>@page{size:landscape;}</style>');
	    mywindow.document.write('</head><body >');
	    mywindow.document.write(content);
	    mywindow.document.write('</body></html>');

	    mywindow.document.close();
	    mywindow.focus()
	    mywindow.print();
	    mywindow.close();
	    return true;
	}
</script>

<?php include_once "inc/footer.php" ?>
