<?php
$getFirstName = explode("-",$last);
$breadcrumbName['key']      = ucwords(str_replace(array('_','.php'),' ',$getFirstName[0]));
$breadcrumbName['value']    = ucwords(str_replace(array('_','-','.php'), " ",$getFirstName[1]));
?>
<ol class="breadcrumb pull-right">
	<li><a href="javascript:;">Home</a></li>
	<li><a href="javascript:;"><?php echo $breadcrumbName['key']; ?></a></li>
	<li class="active"><?php echo (strpos($breadcrumbName['value'],'?') > -1) ? "" : $breadcrumbName['value'];  ?></li>
</ol>
