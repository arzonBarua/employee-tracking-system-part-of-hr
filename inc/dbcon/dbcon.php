<?php
require_once "configuration.php";
class DBCon extends Configuration{
    public $server, $username, $password, $database_name;

    public function __construct(){
        $this->server	    = self::SERVER_NAME;
        $this->username		= self::USER_NAME;
        $this->password		= self::PASSWORD;
        $this->database_name= self::DB_NAME;

        $this->con = null;
    }

    public function getConnection(){
        $this->con = mysqli_connect($this->server, $this->username, $this->password, $this->database_name);
        if (!$this->con) {
            return false;
        }
        return $this->con;
    }

    public function closeConnection(){
        if(isset($this->con)){
            mysqli_close($this->con);
        }
    }
}

$dbCon = new DBCon();
$config = new Configuration();

$con = $dbCon->getConnection();
//mysqli_query($con, 'SET NAMES utf8');
//$dbCon->closeConnection();
?>