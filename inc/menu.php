<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>

			<?php //if ($userType == "Super Admin"): ?>

                <li class="has-sub <?php echo ( ($trek == "user-add.php") || ($trek == "user-view.php")  ) ? "expand" : "" ?>">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-2x fa-users"></i>
                        <span>User</span>
                    </a>
                    <ul class="sub-menu" <?php echo ( ($trek == "user-add.php") || ($trek == "user-view.php") ) ? "style='display:block'" : "" ?> >
                        <li class=<?php echo ($trek == "user-add.php") ? "active" : "" ?> ><a href="user-add.php">Add User</a></li>
                        <li class=<?php echo ($trek == "user-view.php") ? "active" : "" ?> ><a href="user-view.php">View User</a></li>
                    </ul>
                </li>

                <li class="has-sub <?php echo in_array($trek,$pageTracking['company_info'])  ? "expand" : "" ?>">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-2x fa-bank"></i>
                        <span>Company</span>
                    </a>
                    <ul class="sub-menu" <?php echo in_array($trek,$pageTracking['company_info']) ? "style='display:block'" : "" ?>>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Division Information</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "company_division_info-add.php") || ($trek == "company_division_info-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "company_division_info-add.php") ? "active" : "" ?>><a href="company_division_info-add.php">Add Division</a></li>
                                <li class=<?php echo ($trek == "company_division_info-view.php") ? "active" : "" ?>><a href="company_division_info-view.php">View Division</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Company Information</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "company_info-add.php") || ($trek == "company_info-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "company_info-add.php") ? "active" : "" ?>><a href="company_info-add.php">Add Company</a></li>
                                <li class=<?php echo ($trek == "company_info-view.php") ? "active" : "" ?>><a href="company_info-view.php">View Company</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Department</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "department-add.php") || ($trek == "department-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "department-add.php") ? "active" : "" ?>><a href="department-add.php">Add Department</a></li>
                                <li class=<?php echo ($trek == "department-view.php") ? "active" : "" ?>><a href="department-view.php">View Department</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Designation</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "designation-add.php") || ($trek == "designation-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "designation-add.php") ? "active" : "" ?>><a href="designation-add.php">Add Designation</a></li>
                                <li class=<?php echo ($trek == "designation-view.php") ? "active" : "" ?>><a href="designation-view.php">View Designation</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Company Department</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "company_department-add.php") || ($trek == "company_department-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "company_department-add.php") ? "active" : "" ?>><a href="company_department-add.php">Add Company Dept.</a></li>
                                <li class=<?php echo ($trek == "company_department-view.php") ? "active" : "" ?>><a href="company_department-view.php">View Company Dept.</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Company Designation</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "company_designation-add.php") || ($trek == "company_designation-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "company_designation-add.php") ? "active" : "" ?>><a href="company_designation-add.php">Add Company Desig.</a></li>
                                <li class=<?php echo ($trek == "company_designation-view.php") ? "active" : "" ?>><a href="company_designation-view.php">View Company Desig.</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>



                <li class="has-sub <?php echo in_array($trek,$pageTracking['emp_info'])  ? "expand" : "" ?>">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-2x fa-male"></i>
                        <span>Employee</span>
                    </a>
                    <ul class="sub-menu" <?php echo in_array($trek,$pageTracking['emp_info']) ? "style='display:block'" : "" ?>>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Emp Info</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "employee_info-add.php") || ($trek == "employee_info-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "employee_info-add.php") ? "active" : "" ?>><a href="employee_info-add.php">Add Emp Info</a></li>
                                <li class=<?php echo ($trek == "employee_info-view.php") ? "active" : "" ?>><a href="employee_info-view.php">View Emp Info</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Emp Type</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "employee_type-add.php") || ($trek == "employee_type-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "employee_type-add.php") ? "active" : "" ?>><a href="employee_type-add.php">Add Emp Type</a></li>
                                <li class=<?php echo ($trek == "employee_type-view.php") ? "active" : "" ?>><a href="employee_type-view.php">View Emp Type</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Emp Promotion</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "employee_promotion-add.php") || ($trek == "employee_promotion-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "employee_promotion-add.php") ? "active" : "" ?>><a href="employee_promotion-add.php">Add Emp Promotion</a></li>
                                <li class=<?php echo ($trek == "employee_promotion-view.php") ? "active" : "" ?>><a href="employee_promotion-view.php">View Emp Promotion</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>


                <li class="has-sub <?php echo in_array($trek,$pageTracking['location_info'])  ? "expand" : "" ?>">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-2x fa-map-marker"></i>
                        <span>Location</span>
                    </a>
                    <ul class="sub-menu" <?php echo in_array($trek,$pageTracking['location_info']) ? "style='display:block'" : "" ?>>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Division</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "division-add.php") || ($trek == "division-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "division-add.php") ? "active" : "" ?>><a href="division-add.php">Add Division</a></li>
                                <li class=<?php echo ($trek == "division-view.php") ? "active" : "" ?>><a href="division-view.php">View Division</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> District</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "district-add.php") || ($trek == "district-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "district-add.php") ? "active" : "" ?>><a href="district-add.php">Add District</a></li>
                                <li class=<?php echo ($trek == "district-view.php") ? "active" : "" ?>><a href="district-view.php">View District</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Thana</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "thana-add.php") || ($trek == "thana-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "thana-add.php") ? "active" : "" ?>><a href="thana-add.php">Add Thana</a></li>
                                <li class=<?php echo ($trek == "thana-view.php") ? "active" : "" ?>><a href="thana-view.php">View Thana</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="has-sub <?php echo in_array($trek,$pageTracking['education_info'])  ? "expand" : "" ?>">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-2x fa-graduation-cap"></i>
                        <span>Education</span>
                    </a>
                    <ul class="sub-menu" <?php echo in_array($trek,$pageTracking['education_info']) ? "style='display:block'" : "" ?>>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Institution</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "institution-add.php") || ($trek == "institution-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "institution-add.php") ? "active" : "" ?>><a href="institution-add.php">Add Instituion</a></li>
                                <li class=<?php echo ($trek == "institution-view.php") ? "active" : "" ?>><a href="institution-view.php">View Institution</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Degree</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "degree-add.php") || ($trek == "degree-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "degree-add.php") ? "active" : "" ?>><a href="degree-add.php">Add Degree</a></li>
                                <li class=<?php echo ($trek == "degree-view.php") ? "active" : "" ?>><a href="degree-view.php">View Degree</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;"><b class="caret pull-right"></b> Subject</a>
                            <ul class="sub-menu" <?php echo ( ($trek == "subject-add.php") || ($trek == "subject-view.php") ) ? "style='display:block'" : "" ?>>
                                <li class=<?php echo ($trek == "subject-add.php") ? "active" : "" ?>><a href="subject-add.php">Add Subject</a></li>
                                <li class=<?php echo ($trek == "subject-view.php") ? "active" : "" ?>><a href="subject-view.php">View Subject</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li  class='<?php echo ($trek == "beximco-report.php") ? "active" : "";?>'>
                    <a href="beximco-report.php">
                        <i class="fa fa-2x fa-bar-chart"></i>
                        <span>Report</span>
                    </a>
                </li>
			<?php //endif; ?>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>