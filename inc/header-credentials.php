<?php session_start();
ob_start();
/*DB Connection*/
include_once "dbcon/dbcon.php";

/*Login Credential would be here*/

if(isset($_SESSION['id'])){
	$loginId         = $_SESSION['id'];
	$queryString     = "SELECT * FROM `user_info` WHERE `id` = '$loginId'";

	$query           = mysqli_query($con,$queryString);
	$loginRow        = mysqli_fetch_assoc($query);
}else{
	header('Location:index.php');
	exit();
}

list($first, $last) = explode("/",ltrim($_SERVER[REQUEST_URI],"/"));
$trek       = $last;
$trekOther  = strstr($last, '?', true);

if(!empty($trekOther)){
	$trek = $trekOther;
}


$pageTracking['company_info'] = array('company_info-add.php','company_info-view.php','company_division_info-add.php','company_division_info-view.php', 'department-add.php','department-view.php','designation-add.php','designation-view.php','company_department-add.php','company_department-view.php','company_designation-add.php','company_designation-view.php',);
$pageTracking['location_info'] = array('division-add.php', 'division-view.php', 'district-add.php', 'district-view.php', 'thana-add.php', 'thana-view.php');
$pageTracking['education_info'] = array('institution-add.php','institution-view.php','degree-add.php','degree-view.php','subject-add.php','subject-view.php');
$pageTracking['emp_info'] = array('employee_info-add.php','employee_info-view.php','employee_type-add.php', 'employee_type-view.php', '');

$pageTracking['beximco_report'] = array('beximco_report.php');


//echo '<pre>';
//print_r($pageTracking);
//echo '</pre>';
//die;
?>