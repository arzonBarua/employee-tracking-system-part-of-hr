<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$request = mysqli_query($con,"SELECT company_info.*, division_info.div_name FROM `company_info` JOIN `division_info` ON `company_info`.`division_id` = `division_info`.`id`");
?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">View Company Information:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Company Information</h4>
				</div>
				<div class="panel-body">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>
                    <div class="message"></div>
					<table id="data-table" class="table table-striped table-bordered">
						<thead>
						<tr>
							<th>#</th>
							<th>Division Name</th>
                            <th>Company Name</th>
                            <th>Short Name</th>
                            <th>Status</th>
                            <th>Action</th>
						</tr>
						</thead>
						<tbody>
						<?php $sl = 1;
						while($row = mysqli_fetch_assoc($request)): ?>
							<tr>
								<td><?php echo $sl; ?></td>
								<td><?php echo $row['div_name']; ?></td>
                                <td><?php echo $row['company_name'] ?></td>
                                <td><?php echo $row['company_short_name']; ?></td>
                                <td><?php echo ($row['status'] == '1') ? "<span style='color: green'>Yes</span>" : "<span style='color: red'>No</span>" ?></td>
								<td><a href="#view_<?php echo $row['id']; ?>" data-toggle='modal' class="btn btn-xs btn-icon btn-default eye" title="View"><i class="fa fa-eye"></i></a> /
									<a href="company_info-add.php?id=<?php echo $row['id'];?>" alt="Edit"><i class="fa fa-2x fa-edit"></i></a> /
									<a href="javascript:void(0)" id="<?php echo $row['id'] ?>" alt="Delete" class="delete"><i class="fa fa-2x fa-trash-o"></i></a>
                                    <div class="modal fade" id="<?php echo 'view_' . $row['id']; ?>" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                        ×
                                                    </button>
                                                    <h4 class="modal-title">Details Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th width="20%">Division Name</th>
                                                            <td><?php echo $row['div_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Company Name</th>
                                                            <td><?php echo $row['company_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Company Short Name</th>
                                                            <td><?php echo $row['company_short_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Company Logo</th>
                                                            <td><img src="resource/images/<?php echo $row['company_logo'] ?>" class="img-responsive" style="height: 100px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Registered Address</th>
                                                            <td><?php echo $row['registered_addr']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Operation Address</th>
                                                            <td><?php echo $row['operational_addr']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Factory Address</th>
                                                            <td><?php echo $row['factory_addr']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Status</th>
                                                            <td><?php echo ($row['status'] == '1') ? "<span style='color: green'>Yes</span>" : "<span style='color: red'>No</span>" ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th width="20%">Status Change Date</th>
                                                            <td><?php echo date("m/d/Y",strtotime($row['status_change_date'])); ?></td>
                                                        </tr>

                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
							</tr>
							<?php $sl++;
						endwhile; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->


<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).on('click', '.delete', function (e) {
        var id = $(this).attr('id');
//        alert(id);
        var _this = $(this);
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "post",
                    url: "actions/user/ajax.php",
                    data: {id: id},
                    dataType: 'html',
                    success: function (response){
                        $('.message').html(response);
                        _this.closest('tr').remove();
                    },
                    error: function (response){
                        alert('something went wrong please contact to administrator');
                    }
                });
            }
        });
    });
</script>

<?php include_once "inc/footer.php" ?>
