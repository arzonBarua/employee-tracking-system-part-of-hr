<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$request = mysqli_query($con,"SELECT
								empi.*,
								comi.company_name,
								degi.designation_name,
								dpti.department_name,
								empt.emp_type_name
								FROM
								`employee_info` as empi
								LEFT JOIN company_info AS comi ON ( comi.id = empi.company_id )
								LEFT JOIN designation_info AS degi ON ( degi.id = empi.designation_id )
								LEFT JOIN department_info AS dpti ON ( dpti.id = empi.dept_id )
								LEFT JOIN emp_type AS empt ON ( empt.id = empi.dept_id )");

?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Employee Info List:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">View Employee Info List</h4>
				</div>
				<div class="panel-body">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>
                    <div class="message"></div>
					<table id="data-table" class="table table-striped table-bordered">
						<thead>
						<tr>
							<th>#</th>
							<th>Emp Id</th>
							<th>Name</th>
							<th>Company Name</th>
							<th>Department Name</th>
							<th>Emp Type Name</th>
							<th>Designation Name</th>
							<th>Join In Date</th>
							<th>Blood Group</th>
							<th>Mobile No</th>
							<th>E Mail</th>
							<th>Status</th>
                            <th>Action</th>
						</tr>
						</thead>
						<tbody>
						<?php $sl = 1;
						while($row = mysqli_fetch_assoc($request)): ?>
							<tr>
								<td><?php echo $sl; ?></td>
								<td><?php echo $row['emp_id']; ?></td>
								<td><?php echo $row['first_name'].' '.$row['last_name']; ?></td>
								<td><?php echo $row['company_name']; ?></td>
								<td><?php echo $row['department_name']; ?></td>
								<td><?php echo $row['emp_type_name']; ?></td>
								<td><?php echo $row['designation_name']; ?></td>
								<td><?php echo $row['join_in_date']; ?></td>
								<td><?php echo $row['blood_group']; ?></td>
								<td><?php echo $row['mobile_no']; ?></td>
								<td><?php echo $row['e_mail']; ?></td>
								<td><?php echo $row['status']; ?></td>
								<td>
									<a href="employee_type-add.php?id=<?php echo $row['id'];?>" alt="Edit"><i class="fa fa-2x fa-edit"></i></a> /
									<a href="javascript:;" alt="Print" class="print-btn" data-row-id="<?php echo $row['id']; ?>"><i class="fa fa-2x fa-print"></i></a> /
									<a href="javascript:void(0)" id="<?php echo $row['id'] ?>" alt="Delete" class="delete"><i class="fa fa-2x fa-trash-o"></i></a>
								</td>
							</tr>
							<?php $sl++;
						endwhile; ?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->




<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).on('click', '.delete', function (e) {
        var id = $(this).attr('id');
//        alert(id);
        var _this = $(this);
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "post",
                    url: "actions/employee_type/ajax.php",
                    data: {id: id},
                    dataType: 'html',
                    success: function (response){
                        $('.message').html(response);
                        _this.closest('tr').remove();
                    },
                    error: function (response){
                        alert('something went wrong please contact to administrator');
                    }
                });
            }
		});
		
	});

	$(document).ready(function(){
		$(document).on('click', '.print-btn', function(){
			var id = $(this).data('row-id');

			$.ajax({
				type: "post",
				url: "actions/emp_info/ajax-emp-information-print-view.php",
				data: {id: id},
				dataType: 'html',
				success: function (content){
					PrintProfile(content);
				},
				error: function (response){
					alert('something went wrong please contact to administrator');
				}
			});
		});
	});
	
	function PrintProfile(content)
	{
		var mywindow = window.open('', 'PRINT', 'fullscreen=yes');

		mywindow.document.write(content);
		
		mywindow.document.close(); // necessary for IE >= 10
		mywindow.focus(); // necessary for IE >= 10*/

		mywindow.print();
		mywindow.close();

		return true;
	}
</script>

<?php include_once "inc/footer.php" ?>
