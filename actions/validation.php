<?php
function validation($con, $post){
	foreach ($post as $key => $val){
		$post[$key] = mysqli_real_escape_string($con,trim($val));
	}
	return $post;
}


function ftp_upload($ip, $userName, $passWord, $remoteFile, $localFile){
	$returns        = 0;

	$conn_id        = ftp_connect($ip);
	$ftp_user_name  = $userName;
	$ftp_user_pass  = $passWord;

	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	$upload_36 = ftp_put($conn_id, $remoteFile, $localFile, FTP_BINARY);

	if($upload_36) {
		$returns = 1;
	}
}

function insertQryStr($array, $table_name, $link) {
	array_walk($array, function(&$string) use ($link) {
		$string = mysqli_real_escape_string($link, $string);
	});

	$qryStr = sprintf("INSERT INTO %s (%s) VALUES('%s')",
		$table_name,
		implode(", ", array_keys($array)),
		implode("', '", $array)
	);
	return $qryStr;
}

function updateQryStr($values, $table_name, $field_name, $field_name_val, $link) {
	$updateQryStr = "UPDATE ".$table_name." SET ";

	$i = 1;
	$count = count($values);
	foreach($values as $key=>$val) {
		$updateQryStr .= "`".@mysqli_escape_string($link, $key)."` = '".@mysqli_escape_string($link, $val)."'";
		if($count > $i) $updateQryStr .= ", ";
		$i++;
	}

	$updateQryStr .= " WHERE ".$field_name." = '".$field_name_val."'";
	return $updateQryStr;
}

function delete_previous_file($con, $field_name, $table_name, $id, $path){
	$getFileQuery = mysqli_query($con, "SELECT $field_name FROM $table_name WHERE id = '$id'");
	$getFile = mysqli_fetch_assoc($getFileQuery);

	if(!empty($getFile[$field_name])){
		$targetFiles = $path."".$getFile[$field_name];
		unlink($targetFiles);
	}

}