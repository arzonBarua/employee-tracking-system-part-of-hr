<?php session_start();
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";


if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$id = $_POST['id'];
	$tbl_name = "employee_info";

	$_POST['join_in_date'] = date('Y-m-d',strtotime($_POST['join_in_date']));
	$_POST['dob'] = date("Y-m-d",strtotime($_POST['dob']));

	if (!empty($id)) {
		$_POST['modify_by']   = $_SESSION['id'];
		$_POST['modify_date'] = date("Y-m-d");
		$qryStr = updateQryStr($_POST, $tbl_name, 'id', $id, $con);

		mysqli_query($con, $qryStr);
	}else{
		$_POST['entry_by']   = $_SESSION['id'];
		$_POST['entry_date'] = date("Y-m-d");
		$qryStr = insertQryStr($_POST, $tbl_name, $con);

		mysqli_query($con, $qryStr);
		$id = mysqli_insert_id($con);
	}
	echo $id;
}