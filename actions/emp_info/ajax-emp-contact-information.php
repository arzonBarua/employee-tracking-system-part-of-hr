<?php session_start();
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

	$id = $_POST['id'];
	$tbl_name = "employee_info";

	if($_POST['checkBox'] == "on"){
		$_POST['par_house_no'] = $_POST['pre_house_no'];
		$_POST['par_road_no']  = $_POST['pre_road_no'];
		$_POST['par_district'] = $_POST['pre_district'];
		$_POST['par_thana']    = $_POST['pre_thana'];
		//unset($_POST['checkBox']);
	}

	$_POST['modify_by']   = $_SESSION['id'];
	$_POST['modify_date'] = date("Y-m-d");
	$qryStr = updateQryStr($_POST, $tbl_name, 'id', $id, $con);

	mysqli_query($con, $qryStr);
}