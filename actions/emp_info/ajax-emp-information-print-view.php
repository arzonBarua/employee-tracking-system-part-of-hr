<?php
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";


if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['id'])){
		$id = $_POST['id'];
		
		$result = mysqli_query($con, "SELECT
								empi.*,
								comi.company_name,
								degi.designation_name,
								dpti.department_name,
								empt.emp_type_name
								FROM
								`employee_info` as empi
								LEFT JOIN company_info AS comi ON ( comi.id = empi.company_id )
								LEFT JOIN designation_info AS degi ON ( degi.id = empi.designation_id )
								LEFT JOIN department_info AS dpti ON ( dpti.id = empi.dept_id )
								LEFT JOIN emp_type AS empt ON ( empt.id = empi.dept_id )
								WHERE empi.id = '$id'");
								
								$row = mysqli_fetch_assoc($result);


		$result = mysqli_query($con, "SELECT
								si.subject_name,
								di.degree_name,
								ii.institution_name,
								eei.passing_year,
								eei.grade_marks
								FROM
								emp_education_info as eei
								LEFT JOIN subject_info AS si ON ( si.id = eei.subject_id )
								LEFT JOIN degree_info AS di ON ( di.id = eei.degree_id )
								LEFT JOIN institution_info AS ii ON ( ii.id = eei.institution_id )");
		
		$education_info = mysqli_fetch_assoc($result);
		
		$result = mysqli_query($con, "SELECT
								eri.relation,
								eri.remarks,
								empi.emp_id,
								empi.first_name,
								empi.last_name
								FROM
								`emp_relative_info` as eri
								LEFT JOIN employee_info AS empi ON ( empi.id = eri.relative_emp_id )");
		
		$relative_info = mysqli_fetch_assoc($result);
		
		$result = mysqli_query($con, "SELECT * FROM emp_previous_comp_experience");
		
		$job_history = mysqli_fetch_assoc($result);

	
	?>
	<html>

	<head>
		<style>
		.title{
			text-align: center;
		}
		.table{
			width:100%;
		}
		.table td{
			text-align: left;
		}
		fieldset{
			margin-bottom: 10px;
		}
		legend{
			font-size: 12px;
			font-weight: bold;
			border: 1px solid black;
			padding: 2px 10px 2px 10px;
			background-color: lightgrey;
		}

		.border {
			border-collapse: collapse;
			font-size: 12px;
		}

		.border, .border th, .border td {
			border: 1px solid black;
		}
		</style>
	</head>

	<body>
		<h3 class="title">Employee Information</h3>
		<fieldset>
			<legend>Official Information</legend>
			<table class="table">
				<tr>
					<td width="30%">Employee ID </td>
					<td width="1%">:</td>
					<td>
						<?=$row['emp_id']?>
					</td>
					<td rowspan="7" style="text-align: right;">
						<img src="assets/img/Screenshot_6.png" alt="Profile Image">
					</td>
				</tr>
				<tr>
					<td width="30%">Existing ID</td>
					<td width="1%">:</td>
					<td>
						<?=$row['existing_id']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Employee Name</td>
					<td width="1%">:</td>
					<td>
						<?=$row['first_name'].' '.$row['last_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Company Name </td>
					<td width="1%">:</td>
					<td>
						<?=$row['company_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Department</td>
					<td width="1%">:</td>
					<td>
						<?=$row['department_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Designation</td>
					<td width="1%">:</td>
					<td>
						<?=$row['designation_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Joining</td>
					<td width="1%">:</td>
					<td>
						<?=$row['join_in_date']?>
					</td>
				</tr>
			</table>
		</fieldset>

		<fieldset>
			<legend>Personal Information</legend>
			<table class="table">
				<tr>
					<td width="30%">Gender </td>
					<td width="1%">:</td>
					<td>
						<?=$row['gender']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Nationality </td>
					<td width="1%">:</td>
					<td>
						<?=$row['nationality']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Passport No. </td>
					<td width="1%">:</td>
					<td>
						<?=$row['passport_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">NID </td>
					<td width="1%">:</td>
					<td>
						<?=$row['nid']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Tin No.</td>
					<td width="1%">:</td>
					<td>
						<?=$row['tin_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Marital Status</td>
					<td width="1%">:</td>
					<td>
						<?=$row['marital_status']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Religion</td>
					<td width="1%">:</td>
					<td>
						<?=$row['religion']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Dob</td>
					<td width="1%">:</td>
					<td>
						<?=$row['dob']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Place of Birth</td>
					<td width="1%">:</td>
					<td>
						<?=$row['place_of_birth']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Home District</td>
					<td width="1%">:</td>
					<td>
						<?=$row['home_district']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Blood Group</td>
					<td width="1%">:</td>
					<td>
						<?=$row['blood_group']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Mobile No.</td>
					<td width="1%">:</td>
					<td>
						<?=$row['mobile_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Residence Contact No.</td>
					<td width="1%">:</td>
					<td>
						<?=$row['residence_contact_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">E-Mail </td>
					<td width="1%">:</td>
					<td>
						<?=$row['e_mail']?>
					</td>
				</tr>
			</table>
		</fieldset>


		<fieldset>
			<legend>Present Address</legend>
			<table class="table">
				<tr>
					<td width="30%">House No. </td>
					<td width="1%">:</td>
					<td>
						<?=$row['pre_house_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Road No. </td>
					<td width="1%">:</td>
					<td>
						<?=$row['pre_road_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Thana </td>
					<td width="1%">:</td>
					<td>
						<?=$row['pre_thana']?>
					</td>
				</tr>
				<tr>
					<td width="30%">District </td>
					<td width="1%">:</td>
					<td>
						<?=$row['pre_district']?>
					</td>
				</tr>
			</table>
		</fieldset>



		<fieldset>
			<legend>Permanent Address</legend>
			<table class="table">
				<tr>
					<td width="30%">House No. </td>
					<td width="1%">:</td>
					<td>
						<?=$row['par_house_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Road No.</td>
					<td width="1%">:</td>
					<td>
						<?=$row['par_road_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Thana </td>
					<td width="1%">:</td>
					<td>
						<?=$row['par_thana']?>
					</td>
				</tr>
				<tr>
					<td width="30%">District</td>
					<td width="1%">:</td>
					<td>
						<?=$row['par_district']?>
					</td>
				</tr>
			</table>
		</fieldset>

		<fieldset>
			<legend>Family Information</legend>
			<table class="table">
				<tr>
					<td width="30%">Fathers</td>
					<td width="1%">:</td>
					<td>
						<?=$row['fathers_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Mothers </td>
					<td width="1%">:</td>
					<td>
						<?=$row['mothers_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Spouse Name</td>
					<td width="1%">:</td>
					<td>
						<?=$row['spouse_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Spouse contact No.</td>
					<td width="1%">:</td>
					<td>
						<?=$row['spouse_contact_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">No of children</td>
					<td width="1%">:</td>
					<td>
						<?=$row['no_of_children']?>
					</td>
				</tr>
			</table>
		</fieldset>

		<fieldset>
			<legend>Education Information</legend>
			<table class="table border">
				<tr>
					<th>Degree </th>
					<th>Subject</th>
					<th>Institute</th>
					<th>Passing Year</th>
					<th>Grade/Marks</th>
				</tr>
				<?php foreach($education_info as $data){ ?>	
				<tr>
					<td><?=$data['degree_name']?></td>
					<td><?=$data['subject_name']?></td>
					<td><?=$data['institution_name']?></td>
					<td><?=$data['passing_year']?></td>
					<td><?=$data['grade_mark']?></td>
				</tr>
				<?php } ?>	
			</table>
		</fieldset>
		<fieldset>
			<legend>Relative Information</legend>
			<table class="table border">
				<tr>
					<th>Employee Id</th>
					<th>Employee Name</th>
					<th>Relation</th>
					<th>Remarks</th>
				</tr>		
				<?php foreach($relative_info as $data){ ?>	
				<tr>
					<td><?=$data['emp_id']?></td>
					<td><?=$data['first_name']?> <?=$data['last_name']?></td>
					<td><?=$data['relation']?></td>
					<td><?=$data['remarks']?></td>
				</tr>	
				<?php } ?>	
			</table>
		</fieldset>
		<fieldset>
			<legend>Job History</legend>
			<table class="table border">
				<tr>
					<th>Company name</th>
					<th>Department</th>
					<th>Designation</th>
					<th>From Date</th>
					<th>To Date</th>
				</tr>		
				<?php foreach($job_history as $data){ ?>
				<tr>
					<td><?=$data['company_name']?></td>
					<td><?=$data['department_name']?></td>
					<td><?=$data['designation_name']?></td>
					<td><?=$data['from_date']?></td>
					<td><?=$data['to_date']?></td>
				</tr>
				<?php } ?>	
			</table>
		</fieldset>


		<fieldset>
			<legend>Other Information</legend>
			<table class="table">
				<tr>
					<td width="30%">Reference Employee ID</td>
					<td width="1%">:</td>
					<td>
						<?=$row['reference_emp_id']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Reference Name </td>
					<td width="1%">:</td>
					<td>
						<?=$row['reference_name']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Reference Details</td>
					<td width="1%">:</td>
					<td>
						<?=$row['reference_details']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Emergency Contact Person</td>
					<td width="1%">:</td>
					<td>
						<?=$row['emergency_contact_person']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Emergency Contact No</td>
					<td width="1%">:</td>
					<td>
						<?=$row['emergency_contact_no']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Remarks</td>
					<td width="1%">:</td>
					<td>
						<?=$row['remarks']?>
					</td>
				</tr>
				<tr>
					<td width="30%">Status</td>
					<td width="1%">:</td>
					<td>
						<?=$row['status']?>
					</td>
				</tr>
			</table>
		</fieldset>
	</body>

	</html>

	<?php
		
	}
}

?>