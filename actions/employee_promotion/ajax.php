<?php include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['id'])){
		$id = $_POST['id'];
		$query_delete = "DELETE FROM `emp_promotion_info` WHERE id = '$id'";
		$result_delete = mysqli_query($con,$query_delete);
		echo "<div class='alert alert-danger'>Data has been removed Successfully</div>";
	}
}

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['emp_id']) && isset($_POST['dept_id'])){
		$emp_id = $_POST['emp_id'];
		$dept_id = $_POST['dept_id'];

		$result = mysqli_query($con, "SELECT * FROM employee_info WHERE id = '$emp_id'");
		$row = mysqli_fetch_assoc($result);

		$company_id = $row['company_id'];

		$query = mysqli_query($con, "SELECT cd.id, di.department_name FROM company_department as cd LEFT JOIN department_info as di on(di.id = cd.dept_id) WHERE cd.company_id = 1 ORDER BY di.department_name ASC");

		
		$options  = '<option value="">Select Department</option>';
		while($rowList = mysqli_fetch_assoc($query))
		{
			$selected = ($dept_id == $rowList['id'])?'selected':'';
			$options .= '<option value="'.$rowList['id'].'" '.$selected.'>'.$rowList['department_name'].'</option>';
		}
		echo $options;
	}
}

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['emp_id']) && isset($_POST['designation_id'])){
		$emp_id = $_POST['emp_id'];
		$designation_id = $_POST['designation_id'];

		$result = mysqli_query($con, "SELECT * FROM employee_info WHERE id = '$emp_id'");
		$row = mysqli_fetch_assoc($result);

		$company_id = $row['company_id'];

		$query = mysqli_query($con, "SELECT cd.id, di.designation_name FROM company_designation as cd LEFT JOIN designation_info as di on(di.id = cd.designation_id) WHERE cd.company_id = 1 ORDER BY di.designation_name ASC");

		
		$options  = '<option value="">Select Designation</option>';
		while($rowList = mysqli_fetch_assoc($query))
		{
			$selected = ($designation_id == $rowList['id'])?'selected':'';
			$options .= '<option value="'.$rowList['id'].'" '.$selected.'>'.$rowList['designation_name'].'</option>';
		}
		echo $options;
	}
}