<?php
session_start();
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";




if(isset($_POST['submit'])) {
	//$_POST = validation($con,$_POST);
	$id = $_POST['id'];
	unset($_POST['submit']);
	unset($_POST['id']);
	$tbl_name = "emp_promotion_info";
	$edit = 0;

	if(isset($_POST['profile_edit'])){
		$edit = 1;
		unset($_POST['profile_edit']);
	}

	// echo var_dump($id); die;

	if (!empty($id)) {
		$qryStr = updateQryStr($_POST, $tbl_name, 'id', $id, $con);
		$message = "Data Updated Successfully";
	}
	else {
		$qryStr = insertQryStr($_POST, $tbl_name, $con);
		$message = "Data Inserted Successfully";
	}

	if (!mysqli_query($con, $qryStr))
	{
		echo("Error description: " . mysqli_error($con));
	} 

	$_SESSION['success'] = "<div class='alert alert-success'>" . $message . "</div>";


	header('location:../../employee_promotion-view.php');
	exit();

}