<div class="panel panel-inverse" data-sortable-id="form-validation-1">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
			   data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
			   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
			   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
			   data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Education</h4>
	</div>
	<div class="panel-body panel-form">
		<table class="table table-bordered" style="margin-top: 20px;">
			<thead>
			<tr>

				<th width="20px">Institution <br> Name</th>
				<th width="20px">Degree</th>
				<th width="20px">Education <br>Type</th>
				<th width="20px">Subject</th>
				<th width="20px">Passing <br> Year</th>
				<th width="20px">Grade <br>Marks</th>
				<th width="20px">Remarks</th>
				<th width="20px">Action</th>
			</tr>
			</thead>
			<tbody>
			<tr>

				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td><a href="javascript:void(0)" id="1" alt="Delete" class="delete"><i
							class="fa fa-2x fa-trash-o"></i></a></td>
			</tr>

			<tr>
				<td colspan="8" style="text-align: center">
					<a href="javascript:;" class="btn btn-primary add_education">Add Education
						Information</a>
				</td>
			</tr>

			</tbody>
		</table>
	</div>
</div>

<div class="panel panel-inverse" data-sortable-id="form-validation-1">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
			   data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
			   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
			   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
			   data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Experience</h4>
	</div>
	<div class="panel-body panel-form">
		<table class="table table-bordered" style="margin-top: 20px;">
			<thead>
			<tr>

				<th width="20px">Company <br> Name</th>
				<th width="20px">Department<br> Name</th>
				<th width="20px">From Date</th>
				<th width="20px">To Date</th>
				<th width="20px">Remarks</th>
				<th width="20px">Action</th>
			</tr>
			</thead>
			<tbody>
			<tr>

				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>
					<a href="javascript:void(0)" id="1" alt="Delete" class="delete"><i
							class="fa fa-2x fa-trash-o"></i></a>
				</td>
			</tr>

			<tr>
				<td colspan="7" style="text-align: center">
					<a href="javascript:;" class="btn btn-primary add_experience">Add Experience
						Information</a>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="panel panel-inverse" data-sortable-id="form-validation-1">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
			   data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
			   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
			   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
			   data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Relative Information</h4>
	</div>
	<div class="panel-body panel-form">
		<table class="table table-bordered" style="margin-top: 20px;">
			<thead>
			<tr>
				<th width="20px">Relative <br> Employee ID</th>
				<th width="20px">Degree</th>
				<th width="20px">Relation</th>
				<th width="20px">Remarks</th>
				<th width="20px">Action</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td>test</td>
				<td><a href="javascript:void(0)" id="1" alt="Delete" class="delete"><i
							class="fa fa-2x fa-trash-o"></i></a></td>
			</tr>

			<tr>
				<td colspan="5" style="text-align: center">
					<a href="javascript:;" class="btn btn-primary add_relative">Add Relative Information</a>
				</td>
			</tr>

			</tbody>
		</table>
		<div class="col-md-12" style="margin-top: 20px;">
			<p class="text-right m-b-0">
				<a href="javascript:;" class="btn btn-white m-r-5" id="tab3" >Back</a>
			</p>
		</div>
	</div>
</div>