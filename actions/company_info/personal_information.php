<?php
$gender = array("male","female");
$religion = array("Islam","Hindu","Buddhism","Christian");
$bloodGroup = array("ab+","ab-","a+","a-","b+","b-","o+","o-");
$maritalSatus = array("Single","Married","Divorced","Separated","Widowed");

?>
<form class="emp-info" id="personal_information" method="post" data-parsley-validate="true" name="demo-form">
    <div class="col-md-12">
        <!--official Information -->
        <div class="information">
            <div class="row">
                <h4>Official Information</h4><hr>
            </div>
            <div class="row">
                <input type="hidden" name="id" class="emp_main_id" value="<?php echo !empty($id) ? $id : "";  ?>">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="company_id">Company<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm company_id" name="company_id" required>
                                <option value="">-- Select Company--</option>
                                <?php while($row = mysqli_fetch_assoc($companyQuery)): ?>
                                    <option value="<?php echo $row['id'] ?>" <?php if(isset($rowEmplnfo['company_id'])){ echo ($rowEmplnfo['company_id'] == $row['id']) ? "Selected" : ""; }  ?> ><?php echo $row['company_name'] ?></option>
                                <?php endwhile; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="dept_id">Department<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm dept_id" name="dept_id" required>
                                <option value="">-- Select Department --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="emp_type_id">Employee Type<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm" name="emp_type_id" required>
                                <option value="">-- Employee Type --</option>
                                <?php while($row = mysqli_fetch_assoc($empTypeQuery)): ?>
                                    <option value="<?php echo $row['id'] ?>" <?php if(isset($rowEmplnfo['emp_type_id'])){ echo ($rowEmplnfo['emp_type_id'] == $row['id']) ? "Selected" : "";  ?> ><?php echo $row['emp_type_name']; } ?></option>
                                <?php endwhile; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="designation_id">Designation<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm designation_id designation_id" name="designation_id" required>
                                <option value="">-- Select Designation --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="existing_id">Existing ID</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="existing_id" placeholder="Existing ID" class="form-control" name="existing_id" value="<?php echo isset($rowEmplnfo['existing_id']) ? $rowEmplnfo['existing_id'] :""; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="emp_id">Employee ID<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="emp_id" placeholder="Employee ID" class="form-control" name="emp_id" value="<?php echo isset($rowEmplnfo['emp_id']) ? $rowEmplnfo['emp_id'] :""; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="join_in_date">Join in Date<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="join_in_date" placeholder="Join in Date"
                                   class="form-control datepicker" name="join_in_date" value="<?php echo isset($rowEmplnfo['join_in_date']) ? date("m/d/Y",strtotime($rowEmplnfo['join_in_date'])) :""; ?>" autocomplete="off" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End official Information-->

        <!--Personal Information -->
        <div class="information">
            <div class="row">
                <h4>Personal Information</h4><hr>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="firstName">First Name<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="first_name" placeholder="First Name" class="form-control" name="first_name" value="<?php echo isset($rowEmplnfo['first_name']) ? $rowEmplnfo['first_name'] :""; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="lastName">Middle Name</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="middle_name" placeholder="Middle Name" class="form-control" name="middle_name" value="<?php echo isset($rowEmplnfo['middle_name']) ? $rowEmplnfo['middle_name'] :""; ?>" >
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="fatherName">Last Name<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="last_name" placeholder="Last Name" class="form-control" name="last_name" value="<?php echo isset($rowEmplnfo['last_name']) ? $rowEmplnfo['last_name'] :""; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="gender">Gender <span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm" name="gender" required>
                                <option value="">-- Select Gender --</option>
                                <?php foreach ($gender as $value): ?>
                                    <option value="<?php echo $value ?>" <?php if(isset($rowEmplnfo['gender'])){ echo ($rowEmplnfo['gender'] == $value) ? "Selected" : "" ; }  ?> >Male</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="firstName">Father's name<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="fathers_name" placeholder="Father's name" class="form-control"
                                   name="fathers_name" value="<?php echo isset($rowEmplnfo['fathers_name']) ? $rowEmplnfo['fathers_name'] :""; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="lastName">Mother's name<span
                                        class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="mothers_name" placeholder="Mother's name" class="form-control"
                                   name="mothers_name" value="<?php echo isset($rowEmplnfo['mothers_name']) ? $rowEmplnfo['mothers_name'] :""; ?>" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="nationality">Nationality<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm" name="nationality" required>
                                <option value="">-- Select Nationality --</option>
								<?php foreach ($countryArr as $key => $value) { ?>
                                    <option value="<?php echo $value[0]; ?>" <?php if(isset($rowEmplnfo['nationality'])) { echo ($rowEmplnfo['nationality'] == $value[0]) ? "Selected" : ""; }  ?> ><?php echo $value[1]; ?></option>
								<?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="nationalId">National ID No</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="nationalId" placeholder="National ID No" class="form-control" name="nid" value="<?php echo isset($rowEmplnfo['nid']) ? $rowEmplnfo['nid'] :""; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="PassportNo">Passport No</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="passport_no" placeholder="Passport No" class="form-control" name="passport_no" value="<?php echo isset($rowEmplnfo['passport_no']) ? $rowEmplnfo['passport_no'] :""; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="DrivingLicense">Tin No</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="tin_no" placeholder="Tin No" class="form-control" name="tin_no" value="<?php echo isset($rowEmplnfo['tin_no']) ? $rowEmplnfo['tin_no'] :""; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="PassportNo">Date of Birth<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="dob" placeholder="Date of Birth"
                                   class="form-control datepicker" name="dob" autocomplete="off" value="<?php echo isset($rowEmplnfo['dob']) ? date("m/d/Y",strtotime($rowEmplnfo['dob'])) :""; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="DrivingLicense">Place of Birth</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="place_of_birth" placeholder="Place of Birth" class="form-control" name="place_of_birth" value="<?php echo isset($rowEmplnfo['place_of_birth']) ? $rowEmplnfo['place_of_birth'] :""; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="gender">Religion <span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm" name="religion" required>
                                <option value="">-- Select Religion --</option>
                                <?php foreach ($religion as $value): ?>
                                    <option value="<?php echo $value ?>" <?php if(isset($rowEmplnfo['religion'])) { echo ($rowEmplnfo['religion'] == $value) ? "Selected" : ""; } ?> ><?php echo $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="bloodGroup">Blood Group <span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control input-sm" name="blood_group" required>
                                <option value="">-- Select Blood Group --</option>
                                <?php foreach ($bloodGroup as $value): ?>
                                    <option value="<?php echo $value ?>" <?php if(isset($rowEmplnfo['blood_group'])) { echo ($rowEmplnfo['blood_group'] == $value) ? "Selected" : ""; } ?> ><?php echo $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="gender">Marital Status <span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control marital_status input-sm marital_status" id="marital_status" name="marital_status" required>
                                <option value="">-- Select Marital Status --</option>
                                <?php foreach($maritalSatus as $value): ?>
                                    <option value="<?php echo $value ?>" <?php if(isset($rowEmplnfo['marital_status'])) { echo ($rowEmplnfo['marital_status'] == $value) ? "Selected" : ""; } ?> ><?php echo $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 marital_option">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="mobile_no">Spouse Name<span class="required">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="spouse_name" placeholder="Spouse Name" class="form-control spous" name="spouse_name" value="<?php echo isset($rowEmplnfo['spouse_name']) ? $rowEmplnfo['spouse_name'] :""; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row marital_option">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="DrivingLicense">Spouse Contact No</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="spouse_contact_no" placeholder="Spouse Contact No" class="form-control spous" name="spouse_contact_no" value="<?php echo isset($rowEmplnfo['spouse_contact_no']) ? $rowEmplnfo['spouse_contact_no'] :""; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 marital_option">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="mobile_no">No of Children</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="no_of_children" placeholder="No of Children" class="form-control spous"
                                   name="no_of_children" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Personal Information-->

        <!--Reference Information-->
        <div class="information">
            <div class="row">
                <h4>Reference</h4><hr>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="DrivingLicense">Reference Employee ID</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="reference_emp_id" placeholder="Reference Employee ID"
                                   class="form-control"
                                   name="reference_emp_id" value="<?php echo isset($rowEmplnfo['reference_emp_id']) ? $rowEmplnfo['reference_emp_id'] :""; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="mobile_no">Reference Name</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="reference_name" placeholder="Reference Name" class="form-control"
                                   name="reference_name" value="<?php echo isset($rowEmplnfo['reference_name']) ? $rowEmplnfo['reference_name'] :""; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row form-group">
                        <div class="col-md-2">
                            <label for="homePhone">Reference Details</label>
                        </div>
                        <div class="col-md-9">
                        <textarea name="reference_details" class="form-control"
                                  placeholder="Reference Details"><?php echo isset($rowEmplnfo['reference_details']) ? $rowEmplnfo['reference_details'] :""; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 20px">
                <p class="text-right m-b-0">
                    <button type="submit" class="btn btn-primary">Save & Continue</button>
                </p>
            </div>
        </div>

    </div>
</form>