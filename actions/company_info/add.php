<?php
session_start();
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";

if(isset($_POST['submit'])) {
	$_POST = validation($con,$_POST);
	$id = $_POST['id'];
	unset($_POST['submit']);
	unset($_POST['id']);
	$tbl_name = "company_info";
	$edit = 1;
	$uploadOk = 1;

	$condition = "";

	if(!empty($_FILES["company_logo"]["name"])){
		$extensions = array('jpg','png','jepg');
		$filename = $_FILES["company_logo"]["name"];
		$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

		if(!in_array($ext,$extensions)){
			$uploadOk = 0;
			$message = 'File Type Miss Match';
		}

		if (!empty($id)) {
			$condition = "?id=".$id;
		}

	}

	if($uploadOk) {

		$_POST['status_change_date'] = date('Y-m-d',strtotime($_POST['status_change_date']));

		if (!empty($id)) {
			$_POST['modify_by']   = $_SESSION['id'];
			$_POST['modify_date'] = date("Y-m-d");

			$qryStr = updateQryStr($_POST, $tbl_name, 'id', $id, $con);
			$message = "Data Updated Successfully";
			$edit = 0;
		}else {
			$_POST['entry_by']   = $_SESSION['id'];
			$_POST['entry_date'] = date("Y-m-d");

			$qryStr = insertQryStr($_POST, $tbl_name, $con);
			$message = "Data Inserted Successfully";
		}

		mysqli_query($con, $qryStr);

		if ($edit) {
			$id = mysqli_insert_id($con); //Get the last insert Id if insert data
		}

		if(!empty($_FILES["company_logo"]["name"])){
			$path = "../../resource/images/";
			delete_previous_file($con,'company_logo','company_info',$id,$path);

			//move upload file
			$generateFileName = uniqid().".".$ext;
			$target_file = $path."".$generateFileName;

			move_uploaded_file($_FILES["company_logo"]["tmp_name"], $target_file);

			$images['company_logo'] = $generateFileName;
			$qryStr = updateQryStr($images, $tbl_name, 'id', $id, $con);

			mysqli_query($con, $qryStr);
		}


		$_SESSION['success'] = "<div class='alert alert-success'>" . $message . "</div>";
		header('location:../../company_info-view.php');
		exit();
	}else{
		$_SESSION['success'] = "<div class='alert alert-danger'>" . $message . "</div>";
		header('location:../../company_info-add.php'.$condition);
		exit();
	}






}