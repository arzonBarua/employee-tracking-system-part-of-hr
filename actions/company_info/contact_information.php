<form class="emp-info" id="contact_information" method="post" data-parsley-validate="true" name="demo-form">
    <input type="hidden" name="id" class="emp_main_id" value="<?php echo !empty($id) ? $id : "";  ?>">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="mobile_no">Mobile Number<span class="required">*</span></label>
					</div>
					<div class="col-md-6">
						<input type="text" id="mobileNumber" placeholder="ex: 017XXXXXXXX" class="form-control" name="mobile_no" value="<?php echo isset($rowEmplnfo['mobile_no']) ? $rowEmplnfo['mobile_no'] :""; ?>" required>
						<!--<span class="error-mgs error-con-mob">Incorrect Mobile Number</span>-->
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="residence_contact_no">Residence Contact No.</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="homePhone" placeholder="ex: 017XXXXXXXX" class="form-control" name="residence_contact_no" value="<?php echo isset($rowEmplnfo['residence_contact_no']) ? $rowEmplnfo['residence_contact_no'] :""; ?>">
						<!--<span class="error-mgs error-con-alt-mob">Incorrect Mobile Number</span>-->
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="e_mail">Email<span class="required">*</span></label>
					</div>
					<div class="col-md-6">
						<input type="email" id="Email" placeholder="Email" name="e_mail" value="<?php echo isset($rowEmplnfo['e_mail']) ? $rowEmplnfo['e_mail'] :""; ?>" class="form-control" required>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="home_district">Home District</label>
					</div>
					<div class="col-md-6">
						<select class="form-control marital_status input-sm" name="home_district" >
							<option value="">-- Select Home District --</option>
                            <?php foreach($district as $key => $value): ?>
                            <option value="<?php echo $key ?>" <?php if(isset($rowEmplnfo['home_district'])){ echo ($rowEmplnfo['home_district'] ==$key) ? "Selected" : ""; } ?> ><?php echo $value ?></option>
                            <?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="row form-group">
					<div class="col-md-2">
						<label for="current_locaiton">Current Location</label>
					</div>
					<div class="col-md-9">
                        <textarea name="current_locaiton" class="form-control" placeholder="Current Location"><?php echo isset($rowEmplnfo['current_locaiton']) ? $rowEmplnfo['current_locaiton'] :""; ?></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="pre_house_no">Present House No</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="pre_house_no" placeholder="Present House No" class="form-control" name="pre_house_no" value="<?php echo isset($rowEmplnfo['pre_house_no']) ? $rowEmplnfo['pre_house_no'] :""; ?>">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="pre_road_no">Present Road No</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="pre_road_no" placeholder="Present Road No" class="form-control" name="pre_road_no" value="<?php echo isset($rowEmplnfo['pre_road_no']) ? $rowEmplnfo['pre_road_no'] :""; ?>" >
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="pre_district">Present District</label>
					</div>
					<div class="col-md-6">
						<select class="form-control input-sm district-division districtName" name="pre_district" id="next_pre_thana" >
							<option value="">-- Select District --</option>
							<?php foreach($district as $key => $value): ?>
                                <option value="<?php echo $key ?>" <?php if(isset($rowEmplnfo['pre_district'])){ echo ($rowEmplnfo['pre_district'] ==$key) ? "Selected" : ""; } ?> > <?php echo $value ?> </option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="pre_thana">Present Thana</label>
					</div>
					<div class="col-md-6">
						<select class="form-control input-sm division-thana next_pre_thana" name="pre_thana" >
							<option value="">-- Select Thana --</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="row form-group">
			<div class="col-md-6">
				<input type="checkbox" id="checkboxSame" name="checkBox" <?php if(isset($rowEmplnfo['checkBox'])){ echo ($rowEmplnfo['checkBox'] == "on") ? "checked" : "" ; } ?> ><span class="margin-edit">Same as Permanent Address</span>
			</div>
		</div>
	</div>

	<div class="col-md-12 parmanent-address" style="margin-top: 20px;<?php if(isset($rowEmplnfo['checkBox'])){ echo ($rowEmplnfo['checkBox'] == "on") ? "display:none" : "" ; } ?>" >
		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="par_house_no">Permanent House No</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="par_house_no" placeholder="Permanent House No" class="form-control same-as-present" name="par_house_no" value="<?php echo isset($rowEmplnfo['par_house_no']) ? $rowEmplnfo['par_house_no'] :""; ?>" >
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="par_road_no">Permanent Road No</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="par_road_no" placeholder="Permanent Road No" class="form-control same-as-present" name="par_road_no" value="<?php echo isset($rowEmplnfo['par_road_no']) ? $rowEmplnfo['par_road_no'] :""; ?>" >
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="par_district">Permanent District</label>
					</div>
					<div class="col-md-6">
						<select class="form-control input-sm district-division districtName same-as-present" id="next_par_thana" name="par_district" >
							<option value="">-- Select District --</option>
							<?php foreach($district as $key => $value): ?>
                                <option value="<?php echo $key ?>" <?php if(isset($rowEmplnfo['par_district'])){ echo ($rowEmplnfo['par_district'] ==$key) ? "Selected" : ""; } ?> ><?php echo $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="par_thana">Permanent Thana</label>
					</div>
					<div class="col-md-6">
						<select class="form-control input-sm division-thana next_par_thana same-as-present" name="par_thana" >
							<option value="">-- Select Thana --</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12" style="margin-top: 20px;">
		<div class="row">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="emergency_contact_person">Emergency Contact Person<span class="required">*</span></label>
					</div>
					<div class="col-md-6">
						<input type="text" id="emergency_contact_person" placeholder="Emergency Contact Person" class="form-control" name="emergency_contact_person" value="<?php echo isset($rowEmplnfo['emergency_contact_person']) ? $rowEmplnfo['emergency_contact_person'] :""; ?>" required>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label for="emergency_contact_no">Emergency Contact No.<span class="required">*</span></label>
					</div>
					<div class="col-md-6">
						<input type="text" id="emergency_contact_no" placeholder="Emergency Contact No" class="form-control" name="emergency_contact_no" value="<?php echo isset($rowEmplnfo['emergency_contact_no']) ? $rowEmplnfo['emergency_contact_no'] :""; ?>" required>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12" style="margin-top: 20px;">
		<p class="text-right m-b-0">
			<a href="javascript:;" class="btn btn-white m-r-5" id="tab2" >Back</a>
			<button type="submit" class="btn btn-primary" >Save & Continue</button>
		</p>
	</div>
</form>