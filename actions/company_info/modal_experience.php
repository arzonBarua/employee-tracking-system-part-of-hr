<div class="modal fade" id="myModalExperience" role="dialog">
	<div class="modal-dialog" style="width: 60%;">
		<!-- Modal content-->
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Experience Details</h4>
			</div>
			<div class="modal-body modal-ajax-load">
				<!--Demo Data-->
				<div class="panel-body panel-form">
					<form class="emp-info" data-parsley-validate="true" name="demo-form">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">Company Name<span
													class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<input type="text" id="mobileNumber" placeholder="Institution Name"
											       class="form-control" name="mob_num" value="" required>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">Department Name<span
													class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<input type="text" id="mobileNumber" placeholder="Institution Name"
											       class="form-control" name="mob_num" value="" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">From Date<span class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<div class="input-group input-daterange">
												<input type="text" class="form-control" name="start"
												       placeholder="Date Start">
												<span class="input-group-addon">to</span>
												<input type="text" class="form-control" name="end"
												       placeholder="Date End">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">Remarks<span class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<textarea class="form-control" placeholder="Current Location"></textarea>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-12" style="margin-top: 20px;">
							<p class="text-right m-b-0">
								<a href="javascript:;" class="btn btn-primary">Save</a>
							</p>
						</div>
					</form>
				</div>
				<!--End Demo Data-->
			</div>
		</div>
	</div>
</div>