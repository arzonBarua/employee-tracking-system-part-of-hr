<div class="modal fade" id="myModalRelative" role="dialog">
	<div class="modal-dialog" style="width: 60%;">
		<!-- Modal content-->
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Relative Imformation Details</h4>
			</div>
			<div class="modal-body modal-ajax-load">
				<!--Demo Data-->
				<div class="panel-body panel-form">
					<form class="emp-info" data-parsley-validate="true" name="demo-form">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">Relative Employee ID<span class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<input type="text" id="mobileNumber" placeholder="Institution Name" class="form-control" name="mob_num" value="" required>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row form-group">
										<div class="col-md-4">
											<label for="mobileNumber">Relation<span class="required">*</span></label>
										</div>
										<div class="col-md-6">
											<input type="text" id="mobileNumber" placeholder="Institution Name"
											       class="form-control" name="mob_num" value="" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="row form-group">
										<div class="col-md-2">
											<label for="homePhone">Remarks<span class="required">*</span></label>
										</div>
										<div class="col-md-9">
											<textarea class="form-control" placeholder="Current Location"></textarea>
										</div>
									</div>
								</div>
							</div>


						</div>
						<div class="col-md-12" style="margin-top: 20px;">
							<p class="text-right m-b-0">
								<a href="javascript:;" class="btn btn-primary">Save</a>
							</p>
						</div>
					</form>
				</div>
				<!--End Demo Data-->
			</div>
		</div>
	</div>
</div>