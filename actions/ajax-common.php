<?php
include_once "../inc/dbcon/dbcon.php";
include_once "../validation.php";

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if($_POST['track'] == "company_department"){
		$currentId = $_POST['current_id'];
		$coditionString = "";
		//get company department Information according to comapny id
		$getDepartmentQuery = mysqli_query($con,"SELECT `department_info`.`id`, `department_info`.`department_name` FROM `company_department` JOIN `department_info` ON `department_info`.`id` = `company_department`.`dept_id` WHERE `company_department`.`company_id` = '".$_POST['companyId']."'");

		echo "<option value=''>-- Select Department --</option>";

		while($row = mysqli_fetch_assoc($getDepartmentQuery)){
			if(!empty($currentId) && $currentId == $row['id']){
				$coditionString = "Selected";
			}
			echo "<option value = ".$row['id']." $coditionString >".$row['department_name']."</option>";
		}
	}

	if($_POST['track'] == "company_designation"){
		$currentId = $_POST['current_id'];
		$coditionString = "";
		//get company designation according to company id
		$getDesignationQuery = mysqli_query($con, "SELECT `designation_info`.`id`, `designation_info`.`designation_name` FROM `company_designation` JOIN `designation_info` ON `designation_info`.`id` = `company_designation`.`designation_id` WHERE `company_designation`.`company_id` = '".$_POST['companyId']."'");

		echo "<option value=''>-- Select Designation --</option>";
		while($row = mysqli_fetch_assoc($getDesignationQuery)){
			if(!empty($currentId) && $currentId == $row['id']){
				$coditionString = "Selected";
			}
			echo "<option value = ".$row['id']." $coditionString >".$row['designation_name']."</option>";
		}
	}

	if($_POST['track']=="get_thana"){
		$currentId = $_POST['current_id'];
		$coditionString = "";

		$getThanaQuery = mysqli_query($con, "SELECT `id`, `thana_name` FROM `thana_info` WHERE `district_id` = ".$_POST['companyId']." ");
		echo  "<option value=''>-- Select Thana --</option>";

		while($row = mysqli_fetch_assoc($getThanaQuery)){
			if(!empty($currentId) && $currentId == $row['id']){
				$coditionString = "Selected";
			}
			echo "<option value = ".$row['id']." $coditionString >".$row['thana_name']."</option>";
		}

	}

}