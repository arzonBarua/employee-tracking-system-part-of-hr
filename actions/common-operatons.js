function ajaxCall(company_id, tracking, divClass, currentId){
    $.ajax({
        type:"POST",
        url: "actions/ajax-common.php",
        data: {companyId: company_id, track: tracking, current_id: currentId},
        dataType: "text",
        success: function(response){
            $(divClass).html(response);
        },
        error: function(response){
            alert('something went wrong');
        }
    });
}

$(function(){
    $('.company_id').on('change',function () {
        "use strict";
        var company_id = $(this).val();
        ajaxCall(company_id, "company_department", ".dept_id","");
        ajaxCall(company_id, "company_designation", ".designation_id","");
    });


    $('.districtName').on('change',function () {
       "use strict";
        var districtId = $(this).val();
        var nextDiv = $(this).attr('id');
        ajaxCall(districtId, "get_thana", "."+nextDiv,"");
    });
});