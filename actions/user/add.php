<?php
session_start();
include_once "../../inc/dbcon/dbcon.php";
include_once "../validation.php";

if(isset($_POST['submit'])) {
	//$_POST = validation($con,$_POST);
	$id = $_POST['id'];
	unset($_POST['submit']);
	unset($_POST['id']);
	$tbl_name = "user_info";

	if(isset($_POST['profile_edit'])){
		$edit = 1;
		unset($_POST['profile_edit']);
	}

	if (!empty($id)) {
		if (empty($_POST['user_password'])) {
			unset($_POST['user_password']);
		}
		else {
			$_POST['user_password'] = md5($_POST['user_password']);
		}

		$qryStr = updateQryStr($_POST, $tbl_name, 'id', $id, $con);
		$message = "Data Updated Successfully";
	}
	else {
		$_POST['user_password'] = md5($_POST['user_password']);
		$qryStr = insertQryStr($_POST, $tbl_name, $con);
		$message = "Data Inserted Successfully";
	}

//	echo $qryStr;
//	die;
	mysqli_query($con, $qryStr);


	$_SESSION['success'] = "<div class='alert alert-success'>" . $message . "</div>";


	if ($edit){
		header('location:../../edit-profile.php');
		exit();
	}else{
		header('location:../../user-view.php');
		exit();
	}
}