<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$id = isset($_GET['id']) ? $_GET['id'] : "";

if(!empty($id)){
	$result = mysqli_query($con, "SELECT * FROM user_info WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result);
}

$typeList['super'] = "Super Admin";
$typeList['admin'] = "Admin";
$typeList['user']  = "User";

?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Add User:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Add User Form</h4>
				</div>
				<div class="panel-body panel-form">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>

					<form action="actions/user/add.php" method="post" class="form-horizontal form-bordered">


						<div class="form-group">
							<label class="col-md-3 control-label">User Name</label>
							<div class="col-md-9">
								<input type="hidden" name="id" value="<?php echo $id ?>">
								<input type="text" name="user_name" value="<?php echo isset($row['user_name']) ? $row['user_name'] : ''?>" class="form-control" placeholder="User Name" />
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" name="user_password" class="form-control" placeholder="Password" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">User Type</label>
                            <div class="col-md-9">

                                <select name="user_type" class="form-control" required>
                                    <option value="">Select Type</option>
									<?php foreach($typeList as $key => $val): ?>
                                        <option value="<?php echo $key ?>" <?php if(isset($row['user_type'])) { echo ($row['user_type']==$key) ? 'selected' : ''; } ?>><?php echo $val ?></option>
									<?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Active</label>
                            <div class="col-md-9">
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1" <?php if(isset($row['status'])){ echo ($row['status'] == '1') ? "checked" : ""; } ?> >
                                    Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" <?php if(isset($row['status'])){ echo ($row['status'] == '0') ? "checked" : ""; } ?>>
                                    No
                                </label>
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-9">
								<button type="submit" name="submit" class="btn btn-sm btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->
</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<?php include_once "inc/footer.php" ?>
