<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$request = mysqli_query($con,"SELECT `district_info`.`id`, `div_name`, `district_name` FROM `district_info` LEFT JOIN `division_info` ON `district_info`.`division_id` = `division_info`.`id` ORDER BY district_name ASC");

?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">District List:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">View Company Division Information</h4>
				</div>
				<div class="panel-body">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>
                    <div class="message"></div>
					<table id="data-table" class="table table-striped table-bordered">
						<thead>
						<tr>
							<th>#</th>
							<th>District Name</th>
							<th>Division Name</th>
                            <th>Action</th>
						</tr>
						</thead>
						<tbody>
						<?php $sl = 1;
						while($row = mysqli_fetch_assoc($request)): ?>
							<tr>
								<td><?php echo $sl; ?></td>
								<td><?php echo $row['district_name']; ?></td>
                                <td><?php echo $row['div_name'] ?></td>
								<td>
									<a href="district-add.php?id=<?php echo $row['id'];?>" alt="Edit"><i class="fa fa-2x fa-edit"></i></a> /
									<a href="javascript:void(0)" id="<?php echo $row['id'] ?>" alt="Delete" class="delete"><i class="fa fa-2x fa-trash-o"></i></a>
								</td>
							</tr>
							<?php $sl++;
						endwhile; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->


<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).on('click', '.delete', function (e) {
        var id = $(this).attr('id');
//        alert(id);
        var _this = $(this);
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "post",
                    url: "actions/district/ajax.php",
                    data: {id: id},
                    dataType: 'html',
                    success: function (response){
                        $('.message').html(response);
                        _this.closest('tr').remove();
                    },
                    error: function (response){
                        alert('something went wrong please contact to administrator');
                    }
                });
            }
        });
    });
</script>

<?php include_once "inc/footer.php" ?>
