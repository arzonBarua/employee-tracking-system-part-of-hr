<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";


$id = isset($_GET['id']) ? $_GET['id'] : "";

if (!empty($id)) {
	$result = mysqli_query($con, "SELECT * FROM employee_info WHERE id = '$id'");
	$rowEmplnfo = mysqli_fetch_assoc($result);
}

// Country Array
$countryArr = array();
$sql_country = "SELECT country_name, id FROM `countries` ORDER BY UPPER(`country_name`) ASC";
$qry_country = mysqli_query($con, $sql_country);
while ($data_country = mysqli_fetch_assoc($qry_country)){
	$countryArr[] = array($data_country['id'], $data_country['country_name']);
}

//get Company Name & ID
$companyQuery = mysqli_query($con,"SELECT `id`, `company_name` FROM `company_info` ORDER BY `company_name` ASC");

//get employee type
$empTypeQuery = mysqli_query($con, "SELECT `id`, `emp_type_name` FROM `emp_type` ORDER BY `emp_type_name` ASC");

//Get District
$district = array();
$districtQuery = mysqli_query($con,"SELECT `id`, `district_name` FROM `district_info` ORDER BY `district_name` ASC");
while($row = mysqli_fetch_assoc($districtQuery)){
	$district[$row['id']]   = $row['district_name'];
}

?>

<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
    <!-- end breadcrumb -->

    <!-- begin page-header -->
    <h1 class="page-header">Employee:</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-10">
            <!-- Company Search Box -->
            <ul class="nav nav-pills">
                <li class="tab active"><a href="#nav-pills-tab-1" data-toggle="tab" style="pointer-events: none;">Employee Information</a></li>
                <li class="tab1"><a href="#nav-pills-tab-2" data-toggle="tab" style="pointer-events: none;">Contact Information</a></li>
                <li class="tab2"><a href="#nav-pills-tab-3" data-toggle="tab" style="pointer-events: none;">Education & Experience & Relative Information</a></li>
            </ul>
            <div class="tab-content">
                <!--Personal Information-->
                <div class="tab-pane fade active in" id="nav-pills-tab-1">
                    <div class="panel panel-inverse" data-sortable-id="form-validation-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                                   data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Employee Information</h4>
                        </div>
                        <div class="panel-body panel-form">
                            <?php include_once "actions/company_info/personal_information.php"; ?>
                        </div>
                    </div>
                </div>

                <!--contact Information-->
                <div class="tab-pane fade" id="nav-pills-tab-2">
                    <div class="panel panel-inverse" data-sortable-id="form-validation-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
                                   data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                                   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
                                   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
                                   data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Contact Information</h4>
                        </div>
                        <div class="panel-body panel-form">
                            <?php include_once "actions/company_info/contact_information.php"; ?>
                        </div>
                    </div>
                </div>
                <!-- contact Information -->

                <!--- Employee Education & Experience-->
                <div class="tab-pane fade" id="nav-pills-tab-3">
                    <?php include_once "actions/company_info/edu_per_rel.php"; ?>
                </div>
                <!--- Employee Education -->
            </div>
        </div>
        <!-- End Company Search Box -->
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
<!-- end row -->

<!--Modal For Education-->
<?php include_once 'actions/company_info/modal_education.php'; ?>
<!--End Education Model-->

<!--Modal For Experience-->
<?php include_once 'actions/company_info/modal_experience.php'; ?>
<!--End Education Experience-->

<!--Modal For Relative-->
<?php include_once 'actions/company_info/modal_relative.php'; ?>
<!--End Relative-->


</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->

<script>
    $('.datepicker').datepicker({
        format: "mm/dd/yyyy"
    }).on('change', function (ev) {
        $(this).datepicker('hide');
    });

    $(function () {
        //global operation
        $('.marital_option').hide();

        $(".add_education").on('click', function (e) {
            $('#myModals').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $(".add_experience").on('click', function (e) {
            $('#myModalExperience').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $(".add_relative").on('click', function (e) {
            $('#myModalRelative').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        //Personal Information Save Option
        $('#personal_information').on('submit',function (e) {
            event.preventDefault(e);
            //ajax calling
            var _this = $(this);
            $.ajax({
                type:"POST",
                url: "actions/emp_info/ajax-emp-information.php",
                data: _this.serialize(),
                dataType: "text",
                success: function(response){
                    //alert(response);
                    $('.emp_main_id').val(response);
                    $('#nav-pills-tab-1').removeClass("active in");
                    $('#nav-pills-tab-2').addClass("active in");
                    $('.tab').removeClass('active');
                    $('.tab1').addClass('active');
                },
                error: function(response){
                    alert('error');
                }
            });
        });

        //contact information Save option
        $('#contact_information').on('submit',function (e) {
            event.preventDefault(e);
            //ajax calling
            var _this = $(this);
            $.ajax({
                type:"POST",
                url: "actions/emp_info/ajax-emp-contact-information.php",
                data: _this.serialize(),
                dataType: "text",
                success: function(response){
                    //alert(response);
                    $('#nav-pills-tab-2').removeClass("active in");
                    $('#nav-pills-tab-3').addClass("active in");
                    $('.tab1').removeClass('active');
                    $('.tab2').addClass('active');
                },
                error: function(response){
                    alert('error');
                }
            });
        });

        //contact information back option
        $('#tab2').on('click',function(e){
            $('#nav-pills-tab-1').addClass("active in");
            $('#nav-pills-tab-2').removeClass("active in");
            $('.tab').addClass('active');
            $('.tab1').removeClass('active');
        });


        //edu_exp_rel
        $('#tab3').on('click',function(e){
            $('#nav-pills-tab-2').addClass("active in");
            $('#nav-pills-tab-3').removeClass("active in");
            $('.tab1').addClass('active');
            $('.tab2').removeClass('active');
        });

        //change marital status
        $('.marital_status').on('change',function (e) {
            "use strict";
            var maritalStatus = $(this).val();

            if(maritalStatus != "Single"){
                $('.marital_option').show();
                $('#spouse_name').attr('required','required');
            }else{
                $('.marital_option').hide();
                $('#spouse_name').removeAttr('required');
            }
        });

        $('#checkboxSame').on('click',function () {
            if($("#checkboxSame").prop('checked') == true){
                $('.parmanent-address').hide();
                $('.same-as-present').val("");
            }else{
                $('.parmanent-address').show();
                $('.same-as-present').val("");
            }
        });

    });
</script>
<script src="actions/common-operatons.js"></script>
<!-- For Edit -->
<?php if(!empty($id)):
	$company_id = $rowEmplnfo['company_id'];
	$dept_id = $rowEmplnfo['dept_id'];
	$designation_id = $rowEmplnfo['designation_id'];

	$pre_district = $rowEmplnfo['pre_district'];
	$par_district = $rowEmplnfo['par_district'];

	$pre_thana    = $rowEmplnfo['pre_thana'];
	$par_thana    = $rowEmplnfo['pre_thana'];
	?>
    <script>
        var company_id = "<?php echo $company_id; ?>";
        var dept_id = "<?php echo $dept_id; ?>";
        var designation_id = "<?php echo $designation_id; ?>";

        ajaxCall(company_id, "company_department", ".dept_id",dept_id);
        ajaxCall(company_id, "company_designation", ".designation_id",designation_id);

        var pre_district = "<?php echo $pre_district; ?>";
        var par_district = "<?php echo $par_district; ?>";
        var pre_thana    = "<?php echo $pre_thana; ?>";
        var par_thana    = "<?php echo $par_thana; ?>";

        ajaxCall(pre_district, "get_thana", ".next_pre_thana", pre_thana);
        ajaxCall(par_district, "get_thana", ".next_par_thana", par_thana);
    </script>
<?php endif; ?>
<!--End For Edit-->
<?php include_once "inc/footer.php" ?>
