<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$id = isset($_GET['id']) ? $_GET['id'] : "";

if(!empty($id)){
	$result = mysqli_query($con, "SELECT * FROM emp_promotion_info WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result);
}

$queryGetEmployee = mysqli_query($con,"SELECT * FROM employee_info ORDER BY first_name ASC");


$employeeList  = array();
while($rowList = mysqli_fetch_assoc($queryGetEmployee))
{
	$employeeList[$rowList['id']] = $rowList['first_name'] . ' ' . $rowList['last_name'];
}


?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Employee Promotion Information:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Add Employee Promotion Information:</h4>
				</div>
				<div class="panel-body panel-form">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>


					<form action="actions/employee_promotion/add.php" method="post" class="form-horizontal form-bordered">
						<?php if(!empty($id)): ?>
							<input name="id" type="hidden" value="<?=$id?>">
						<?php endif;?>
						<div class="form-group">
                            <label class="col-md-3 control-label">Employee</label>
                            <div class="col-md-9">
                                <select name="emp_id" class="form-control" required>
                                    <option value="">Select Employee</option>
									<?php foreach($employeeList as $key => $val): ?>
                                        <option value="<?php echo $key ?>" <?php if(isset($row['emp_id'])) { echo ($row['emp_id']==$key) ? 'selected' : ''; } ?>><?php echo $val ?></option>
									<?php endforeach; ?>
                                </select>
                            </div>
                        </div>

						<div class="form-group">
                            <label class="col-md-3 control-label">Department</label>
                            <div class="col-md-9">
                                <select name="dept_id" class="form-control" required>
                                    <option value="">Select Department</option>
                                </select>
                            </div>
                        </div>

						<div class="form-group">
                            <label class="col-md-3 control-label">Designation</label>
                            <div class="col-md-9">
                                <select name="designation_id" class="form-control" required>
                                    <option value="">Select Designation</option>
                                </select>
                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-3 control-label">Promotion Date</label>
							<div class="col-md-9">
								<input type="date" name="promotion_date" value="<?php echo isset($row['promotion_date']) ? $row['promotion_date'] : ''?>" class="form-control" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Remarks</label>
							<div class="col-md-9">
								<input type="text" name="remarks" value="<?php echo isset($row['remarks']) ? $row['remarks'] : ''?>" class="form-control" placeholder="Remarks" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-9">
								<button type="submit" name="submit" class="btn btn-sm btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->
</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script src="assets/js/custom.js"></script>
<script>
    $(document).ready(function(){
		$('select[name="emp_id"]').change(function(){
			var emp_id = $(this).val();

			ajax_update_select('select[name="dept_id"]', {
				emp_id: emp_id,
				dept_id: '<?php if(isset($row['dept_id'])) { echo $row['dept_id']; } ?>'
			}, "actions/employee_promotion/ajax.php");

			ajax_update_select('select[name="designation_id"]', {
				emp_id: emp_id,
				designation_id: '<?php if(isset($row['designation_id'])) { echo $row['designation_id']; } ?>'
			}, "actions/employee_promotion/ajax.php");

		});

		$('select[name="emp_id"]').trigger("change");

	});
</script>

<?php include_once "inc/footer.php" ?>
