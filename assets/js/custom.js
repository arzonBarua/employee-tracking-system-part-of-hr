function ajax_update_select(selector, data, url)
{
    $.ajax({
        type: "post",
        url: "actions/employee_promotion/ajax.php",
        data: data,
        dataType: 'html',
        success: function (response){
            $(selector).html(response);
        },
        error: function (response){
            alert('something went wrong please contact to administrator');
        }
    });

}