<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

$id = isset($_GET['id']) ? $_GET['id'] : "";

if(!empty($id)){
	$result = mysqli_query($con, "SELECT * FROM company_division_info WHERE id = '$id'");
	$row = mysqli_fetch_assoc($result);
}

?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Company Division Information:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-md-8">
			<!-- Company Search Box -->
			<div class="panel panel-inverse" data-sortable-id="form-plugins-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default"
						   data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
						   data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"
						   data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger"
						   data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Add Company Division Information:</h4>
				</div>
				<div class="panel-body panel-form">
					<?php if(isset($_SESSION['success'])): ?>
						<?php echo $_SESSION['success']; unset($_SESSION['success']); ?>
					<?php endif;?>

					<form action="actions/company_division_info/add.php" method="post" class="form-horizontal form-bordered">
						<div class="form-group">
							<label class="col-md-3 control-label">Division Name</label>
							<div class="col-md-9">
								<input type="text" name="division_name" value="<?php echo isset($row['division_name']) ? $row['division_name'] : ''?>" class="form-control" placeholder="Division Name" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label">Division Short Name</label>
							<div class="col-md-9">
								<input type="hidden" name="id" value="<?php echo $id ?>">
								<input type="text" name="division_short_name" value="<?php echo isset($row['division_short_name']) ? $row['division_short_name'] : ''?>" class="form-control" placeholder="Division Short Name" required/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-9">
								<button type="submit" name="submit" class="btn btn-sm btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Company Search Box -->
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
</div>
<!-- end row -->
</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<?php include_once "inc/footer.php" ?>
