<?php include_once "inc/header.php";
include_once "inc/nav-bar.php";
include_once "inc/menu.php";

if (($userType == "Super Admin") || (in_array('update_files', $accessListArray))):
include_once "actions/file-upload/common.php";
?>

<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
    <?php include_once "inc/breadcrumb.php" ?>
	<!-- end breadcrumb -->

	<!-- begin page-header -->
	<h1 class="page-header">Upload Profiles:</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
        <!-- begin col-6 -->
        <div class="col-md-8">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">(Upload Txt File)</h4>
                </div>
                <div class="panel-body">
                    <form action="actions/file-upload/upload.php" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" id="add-company">
                        <?php if(!empty($_SESSION['success'])): ?>
                            <div class="alert alert-success"><?php implode(", ",$_SESSION['success']); unset($_SESSION['success']) ?></div>
                        <?php endif; ?>
                        <?php if(!empty($_SESSION['error'])): ?>
                            <div class="alert alert-danger"><?php implode(", ",$_SESSION['error']); unset($_SESSION['error']) ?></div>
                        <?php endif;?>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Select Profile</label>
                            <div class="col-md-8">
                                <select name="cat" class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-primary" required>
                                    <option value="">-Select File-</option>
				                    <?php foreach($file as $key => $val): ?>
                                        <option value="<?php echo $key ?>"><?php echo $val ?></option>
				                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Select Trading Code</label>
                            <div class="col-md-8">
                                <select  name="symbol" class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-primary" required>
                                    <option value="">-Select Code-</option>
		                            <?php foreach($listCompany as $val): ?>
                                        <option value="<?php echo $val ?>"><?php echo $val ?></option>
		                            <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <input type="file" name="file_upload" class="form-control" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <button type="submit" name="submit" class="btn btn-sm btn-success">Upload File</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
	</div>
	<!-- end row -->
</div>

<?php include_once "inc/footer-link.php" ?>
<!--Space For Custom Js-->
<script>
    $('.datepicker-range').datepicker({
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months"
    }).on('change',function(ev){
        $(this).datepicker('hide');
    });

    $(".datepicker-year").datepicker({
        format: "yyyy",
        startView: "years",
        minViewMode: "years"
    }).on('change',function(ev){
        $(this).datepicker('hide');
    });
</script>
<?php include_once "inc/footer.php" ?>
<?php else:
	header("location: dashboard.php");
endif;
?>